#pragma once

#include "Graphics\Material.h"

class TextureData;

class ModelMaterial : public Material
{
public:
	ModelMaterial();
	~ModelMaterial();

	void SetLightDirection(XMFLOAT3 direction);
	void SetDiffuseTexture(const wstring& assetFile);
	void SetNormalTexture(const wstring& assetFile);

protected:
	void LoadEffectVariables() override;
	void UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent) override;

private:
	XMFLOAT3 m_LightDirection;
	static ID3DX11EffectVectorVariable* m_pLightDirectionVariable;

	TextureData* m_pDiffuseTexture;
	TextureData* m_pNormalTexture;
	static ID3DX11EffectShaderResourceVariable* m_pDiffuseTexSRVariable;
	static ID3DX11EffectShaderResourceVariable* m_pNormalTexSRVariable;

	ModelMaterial(const ModelMaterial &obj) = delete;
	ModelMaterial& operator=(const ModelMaterial& obj) = delete;

};

