#include "Base\stdafx.h"
#include "DiffuseMaterial.h"
#include "Content\ContentManager.h"
#include "Graphics\TextureData.h"
#include "Components\ModelComponent.h"

ID3DX11EffectShaderResourceVariable* DiffuseMaterial::m_pDiffuseSRVariable = nullptr;

DiffuseMaterial::DiffuseMaterial() :
	m_pDiffuseTexture{ nullptr },
	Material(L"Resources/Effects/PosNormTex3D.fx")
{
}

DiffuseMaterial::~DiffuseMaterial()
{
}

void DiffuseMaterial::SetDiffuseTexture(const wstring & assetFile)
{
	m_pDiffuseTexture = ContentManager::Load<TextureData>(assetFile);
}

void DiffuseMaterial::LoadEffectVariables()
{
	m_pDiffuseSRVariable = m_pEffect->GetVariableByName("gDiffuseMap")->AsShaderResource();
}

void DiffuseMaterial::UpdateEffectVariables(const GameContext & gameContext, ModelComponent * pModelComponent)
{
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pModelComponent);
	m_pDiffuseSRVariable->SetResource(m_pDiffuseTexture->GetShaderResourceView());
}
