#include "Base\stdafx.h"
#include "ModelMaterial.h"
#include "Content\ContentManager.h"
#include "Graphics\TextureData.h"
#include "Components\ModelComponent.h"

ID3DX11EffectVectorVariable* ModelMaterial::m_pLightDirectionVariable = nullptr;
ID3DX11EffectShaderResourceVariable* ModelMaterial::m_pDiffuseTexSRVariable = nullptr;
ID3DX11EffectShaderResourceVariable* ModelMaterial::m_pNormalTexSRVariable = nullptr;

ModelMaterial::ModelMaterial() :
	m_pNormalTexture{ nullptr },
	m_pDiffuseTexture{ nullptr },
	m_LightDirection{ 0.f, 0.f ,0.f },
	Material(L"Resources/Effects/ModelTexNorm.fx")
{}

ModelMaterial::~ModelMaterial()
{}

void ModelMaterial::SetLightDirection(XMFLOAT3 direction)
{
	m_LightDirection = direction;
}
void ModelMaterial::SetDiffuseTexture(const wstring & assetFile)
{
	m_pDiffuseTexture = ContentManager::Load<TextureData>(assetFile);
}

void ModelMaterial::SetNormalTexture(const wstring & assetFile)
{
	m_pNormalTexture = ContentManager::Load<TextureData>(assetFile);
}

void ModelMaterial::LoadEffectVariables()
{
	m_pLightDirectionVariable = m_pEffect->GetVariableByName("gLightDirection")->AsVector();

	m_pDiffuseTexSRVariable = m_pEffect->GetVariableByName("gTextureDiffuse")->AsShaderResource();
	m_pNormalTexSRVariable = m_pEffect->GetVariableByName("gNormalMapTexture")->AsShaderResource();
}

void ModelMaterial::UpdateEffectVariables(const GameContext & gameContext, ModelComponent * pModelComponent)
{
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pModelComponent);
	m_pLightDirectionVariable->SetFloatVector(&m_LightDirection.x);

	m_pDiffuseTexSRVariable->SetResource(m_pDiffuseTexture->GetShaderResourceView());
	m_pNormalTexSRVariable->SetResource(m_pNormalTexture->GetShaderResourceView());
}
