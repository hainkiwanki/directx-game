//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "PostBloom.h"
#include "Graphics/RenderTarget.h"

ID3DX11EffectShaderResourceVariable* PostBloom::m_pTextureMapVariabele = nullptr;

PostBloom::PostBloom()
	: PostProcessingMaterial(L"./Resources/Effects/Post/Bloom.fx")
{
}

PostBloom::~PostBloom(void)
{
}

void PostBloom::LoadEffectVariables()
{
	//Bind the 'gTexture' variable with 'm_pTextureMapVariable'
	m_pTextureMapVariabele = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
	//Check if valid!
	if (!m_pTextureMapVariabele)
	{
		Logger::LogWarning(L"PostBloom::LoadEffectVariables() > \'gTexture\' variable not found!");
		m_pTextureMapVariabele = nullptr;
	}
}

void PostBloom::UpdateEffectVariables(RenderTarget* rendertarget)
{
	//Update the TextureMapVariable with the Color ShaderResourceView of the given RenderTarget
	m_pTextureMapVariabele->SetResource(rendertarget->GetShaderResourceView());
}