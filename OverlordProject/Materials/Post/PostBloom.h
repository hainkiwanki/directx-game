#pragma once
#include "Graphics/PostProcessingMaterial.h"

class ID3D11EffectShaderResourceVariable;

class PostBloom : public PostProcessingMaterial
{
public:
	PostBloom();
	~PostBloom();

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget);

	static ID3DX11EffectShaderResourceVariable* m_pTextureMapVariabele;


private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PostBloom(const PostBloom &obj);
	PostBloom& operator=(const PostBloom& obj);
};
