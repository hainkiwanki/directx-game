#pragma once
#include "Graphics/PostProcessingMaterial.h"

class ID3D11EffectShaderResourceVariable;

class PostCustom : public PostProcessingMaterial
{
public:
	PostCustom();
	~PostCustom();

	void SetContrast(float contrast);
	void Toggle4Bit();
	void ToggleInverse();

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget);

	static ID3DX11EffectShaderResourceVariable* m_pTextureMapVariabele;
	float m_Contrast;
	static ID3DX11EffectScalarVariable* m_pContrastVariable;
	bool m_Use4Bit;
	static ID3DX11EffectScalarVariable* m_pUse4BitVariable;
	bool m_Inverse;
	static ID3DX11EffectScalarVariable* m_pInverseVariable;
private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PostCustom(const PostCustom &obj);
	PostCustom& operator=(const PostCustom& obj);
};
