//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "PostCustom.h"
#include "Graphics/RenderTarget.h"

ID3DX11EffectShaderResourceVariable* PostCustom::m_pTextureMapVariabele = nullptr;
ID3DX11EffectScalarVariable* PostCustom::m_pContrastVariable = nullptr;
ID3DX11EffectScalarVariable* PostCustom::m_pUse4BitVariable = nullptr;
ID3DX11EffectScalarVariable* PostCustom::m_pInverseVariable = nullptr;

PostCustom::PostCustom()
	: PostProcessingMaterial(L"./Resources/Effects/Post/Custom.fx"),
	m_Contrast(1.0f),
	m_Use4Bit{false},
	m_Inverse{false} 
{}

PostCustom::~PostCustom(void){}

void PostCustom::SetContrast(float contrast)
{
	m_Contrast = contrast;
}

void PostCustom::Toggle4Bit()
{
	m_Use4Bit = !m_Use4Bit;
}

void PostCustom::ToggleInverse()
{
	m_Inverse = !m_Inverse;
}

void PostCustom::LoadEffectVariables()
{
	//Bind the 'gTexture' variable with 'm_pTextureMapVariable'
	m_pTextureMapVariabele = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
	m_pContrastVariable = m_pEffect->GetVariableByName("gContrast")->AsScalar();
	m_pUse4BitVariable = m_pEffect->GetVariableByName("gUse4Bit")->AsScalar();
	m_pInverseVariable = m_pEffect->GetVariableByName("gInverse")->AsScalar();
	//Check if valid!
	if (!m_pTextureMapVariabele)
	{
		Logger::LogWarning(L"PostCustom::LoadEffectVariables() > \'gTexture\' variable not found!");
		m_pTextureMapVariabele = nullptr;
	}
}

void PostCustom::UpdateEffectVariables(RenderTarget* rendertarget)
{
	//Update the TextureMapVariable with the Color ShaderResourceView of the given RenderTarget
	m_pTextureMapVariabele->SetResource(rendertarget->GetShaderResourceView());
	m_pContrastVariable->SetFloat(m_Contrast);
	m_pUse4BitVariable->SetBool(m_Use4Bit);
	m_pInverseVariable->SetBool(m_Inverse);
}