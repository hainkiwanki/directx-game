#pragma once
#include "Scenegraph\GameObject.h"

class BulletPrefab final : public GameObject
{
public:
	BulletPrefab(const XMFLOAT3& startPos, GameObject* source);
	~BulletPrefab(void);

	void Delete() { m_GettingDeleted = true; };
	bool ToBeDeleted() { return m_GettingDeleted; };

protected:
	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);

private:
	bool m_GettingDeleted = false;
	bool m_IsShot = false;
	float m_BeginPos = 0.0f;
	float m_EndPos = 10.0f;
	float m_Timer = 0.0f;
	GameObject* m_pSource;
	XMFLOAT3 m_Pos;

	FMOD::Sound* m_pRockPunch = nullptr;
	FMOD::Sound* m_pRockSpawn = nullptr;
	FMOD::Sound* m_pRockHit = nullptr;
	FMOD::Channel* m_pChannel = nullptr;

private:
	BulletPrefab(const BulletPrefab& yRef);
	BulletPrefab& operator=(const BulletPrefab& yRef);
};

