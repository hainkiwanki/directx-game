#pragma once
#include "Scenegraph\GameObject.h"

class MenuPrefab final : public GameObject
{
public:
	MenuPrefab(const wstring& path);
	~MenuPrefab(void) = default;

	void AddCursorPosition(XMFLOAT3 pos);
	void Update(const GameContext& gameContext) override;
	void NextPosition();
	void PreviousPosition();
	void Hide();
	void Unhide();

protected:
	virtual void Initialize(const GameContext& gameContext);

private:
	wstring m_Path;
	GameObject* m_pMenuObj;
	GameObject* m_pCursor1;
	GameObject* m_pCursor2;
	bool m_Hidden;
	int m_CurrentPos;
	int m_MaxPos;
	std::vector<XMFLOAT3> m_CursorPos;

	MenuPrefab(const MenuPrefab& yRef);
	MenuPrefab& operator=(const MenuPrefab& yRef);
};