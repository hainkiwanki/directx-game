#include "Base\stdafx.h"
#include "EnemyBullet.h"
#include "Components\Components.h"
#include "..\Overlordproject\Materials\SkinnedDiffuseMaterial.h"
#include "Physx\PhysxManager.h"
#include "..\Overlordproject\Exam\Enums.h"
#include "Scenegraph\GameScene.h"
#include "Prefabs\Prefabs.h"
#include "..\Overlordproject\Exam\smallMaths.h"
#include "Base\SoundManager.h"
#include "..\Overlordproject\Exam\Prefabs\CustomPrefabs.h"

EnemyBullet::EnemyBullet(const XMFLOAT3& startPos, GameObject* source) :
	m_Pos{ startPos }, m_pSource{ source }
{}

EnemyBullet::~EnemyBullet(){}

void EnemyBullet::Initialize(const GameContext & gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	//PHYSX
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto normalMat = physX->createMaterial(1.0f, 1.0f, 0.0f);
	auto fmodSystem = SoundManager::GetInstance()->GetSystem();

	fmodSystem->createSound("Resources/Sounds/Hit.wav", (FMOD_2D | FMOD_DEFAULT), NULL, &m_pRockHit);

	//MESH
	auto pRockObj = new GameObject();
	auto pModel = new ModelComponent(L"Resources/Meshes/Rock.ovm");
	int random = rand() % 100;
	if (random < 50)
		pModel->SetMaterial(34);
	else
		pModel->SetMaterial(35);
	pRockObj->AddComponent(pModel);
	pRockObj->GetTransform()->Scale(2.0f, 2.0f, 2.0f);
	pRockObj->GetTransform()->Translate(0.5f, 0.0f, 0.0f);
	AddChild(pRockObj);

	//RIGIDBODY
	auto pRigidBody = new RigidBodyComponent();
	pRigidBody->SetKinematic(true);
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group1);
	pRigidBody->SetCollisionIgnoreGroups(static_cast<CollisionGroupFlag>(Group1));
	AddComponent(pRigidBody);
	
	//COLLIDER
	std::shared_ptr<PxGeometry> sphereGeom(new PxSphereGeometry(1.0f));
	auto pCollider = new ColliderComponent(sphereGeom, *normalMat, PxTransform(PxIdentity));
	pCollider->EnableTrigger(true);
	AddComponent(pCollider);
	SetTag(L"bullet");

	//TRIGGER CALLBACK LAMBDA
	auto lambda = [=](GameObject * triggerobject, GameObject * otherobject, GameObject::TriggerAction action)
	{
		UNREFERENCED_PARAMETER(triggerobject);
		if (action == GameObject::TriggerAction::ENTER)
		{
			if (otherobject != m_pSource)
			{
				if (otherobject->GetTag() == L"player")
				{
					fmodSystem->playSound(m_pRockHit, NULL, true, &m_pChannel);
					m_pChannel->setVolume(3.0f);
					m_pChannel->setPaused(false);
					static_cast<CharacterPrefab*>(otherobject)->DecreaseHealth();
				}
				Delete();
			}
		}
	};
	PhysicsCallback f_test = lambda;
	SetOnTriggerCallBack(f_test);

	GetTransform()->Translate(m_Pos.x, m_Pos.y, m_Pos.z);
}

void EnemyBullet::Update(const GameContext & gameContext)
{
	auto pos = GetTransform()->GetPosition();
	auto dt = gameContext.pGameTime->GetElapsed();
	pos.z -= 40.0f * dt;
	GetTransform()->Translate(pos.x, pos.y, pos.z);
}