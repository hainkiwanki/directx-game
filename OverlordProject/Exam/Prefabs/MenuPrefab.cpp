#include "Base\stdafx.h"
#include "MenuPrefab.h"
#include <Physx/PhysxProxy.h>
#include "Physx\PhysxManager.h"
#include "Components\Components.h"

MenuPrefab::MenuPrefab(const wstring & path) :
	m_Path{ path }, m_CurrentPos{ 0 }, m_Hidden{ true }, m_MaxPos{ -2 }
{}

void MenuPrefab::AddCursorPosition(XMFLOAT3 pos)
{
	m_CursorPos.push_back(pos);
	m_MaxPos += 1;
}

void MenuPrefab::Update(const GameContext& gameContext)
{
	GameObject::Update(gameContext);
	if (!m_Hidden && m_CursorPos.size() > 0)
	{
		m_pCursor1->GetTransform()->Translate(m_CursorPos.at(m_CurrentPos));
		m_pCursor2->GetTransform()->Translate(m_CursorPos.at(m_CurrentPos + 1));
	}
	else
	{
		m_pCursor1->GetTransform()->Translate(-1000.0f, 0.0f, 0.0f);
		m_pCursor2->GetTransform()->Translate(-1000.0f, 0.0f, 0.0f);
	}
}

void MenuPrefab::NextPosition()
{
	if(m_CurrentPos < m_MaxPos)
		m_CurrentPos += 2;
}

void MenuPrefab::PreviousPosition()
{
	if (m_CurrentPos > 0)
		m_CurrentPos -= 2;
}

void MenuPrefab::Hide()
{
	m_pMenuObj->GetComponent<SpriteComponent>()->SetPivot(XMFLOAT2(1.0f, 1.0f));
	m_Hidden = true;
}

void MenuPrefab::Unhide()
{
	m_pMenuObj->GetComponent<SpriteComponent>()->SetPivot(XMFLOAT2(0.0f, 0.0f));
	m_Hidden = false;
}

void MenuPrefab::Initialize(const GameContext & gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pMenuObj = new GameObject();
	m_pMenuObj->AddComponent(new SpriteComponent(m_Path, XMFLOAT2(1.f, 1.f), XMFLOAT4(1, 1, 1, 1.f)));
	m_pCursor1 = new GameObject();
	m_pCursor1->AddComponent(new SpriteComponent(L"Resources/Textures/Cursor.png", XMFLOAT2(0.f, 0.f), XMFLOAT4(1, 1, 1, 1.f)));
	m_pCursor2 = new GameObject();
	m_pCursor2->AddComponent(new SpriteComponent(L"Resources/Textures/Cursor.png", XMFLOAT2(0.f, 0.f), XMFLOAT4(1, 1, 1, 1.f)));
	AddChild(m_pCursor1);
	AddChild(m_pCursor2);
	AddChild(m_pMenuObj);

	SetTag(L"UI");
}

