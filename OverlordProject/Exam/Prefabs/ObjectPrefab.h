#pragma once
#include "Scenegraph\GameObject.h"

class FloorPanel final : public GameObject
{
public:
	FloorPanel() = default;
	~FloorPanel(void) = default;

protected:
	virtual void Initialize(const GameContext& gameContext);

private:
	FloorPanel(const FloorPanel& yRef);
	FloorPanel& operator=(const FloorPanel& yRef);
};

class WallPanel final : public GameObject
{
public:
	WallPanel(bool withText, bool onRight, const XMFLOAT3& offset) :
		m_WithText(withText), m_OnRight(onRight), m_Offset(offset) {}
	~WallPanel(void) = default;

protected:
	virtual void Initialize(const GameContext& gameContext);

private:
	bool m_WithText = false;
	bool m_OnRight = false;
	XMFLOAT3 m_Offset;

	WallPanel(const WallPanel& yRef);
	WallPanel& operator=(const WallPanel& yRef);
};