#pragma once
#include "Scenegraph\GameObject.h"

class ModelComponent;
class ControllerComponent;
enum class CharacterState;

class CharacterPrefab : public GameObject
{
public:
	CharacterPrefab();
	~CharacterPrefab(void);

	int GetHealth() { return m_Health; }
	void DecreaseHealth() { m_Health--; }
	bool IsAlive() { return m_Health > 0; }
	void Reset();

protected:
	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);

	void SetCharacterState(CharacterState state);

private:
	GameObject* m_pModelObj = nullptr;
	ModelComponent* m_pModel;
	ControllerComponent* m_pCharacterController = nullptr;
	
	XMFLOAT3 m_OriginalPos;
	CharacterState m_State;
	float m_Speed;
	float m_Dir;
	int m_Health;
	bool m_ChangeAnimation;
	bool m_IsAttacking;

	void UpdateState(const GameContext& gameContext);
	void UpdateAnimation();

private:
	CharacterPrefab(const CharacterPrefab& yRef);
	CharacterPrefab& operator=(const CharacterPrefab& yRef);
};

