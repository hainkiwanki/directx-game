#pragma once
#include "Scenegraph\GameObject.h"

class ModelComponent;
class ControllerComponent;
enum class EnemyState;

class EnemyPrefab : public GameObject
{
public:
	EnemyPrefab(int matId, const std::wstring& model, int health);
	~EnemyPrefab(void);

	void DecreaseHealth() { m_Health--; }
	bool IsDead() { return m_Health == 0; }
	int GetScore() { return m_Score; }

protected:
	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);

protected:
	int m_Health;
	int m_ShootChance;
	int m_Score;

private:
	int m_MatId;
	float m_Speed;
	float m_NewSpeed;
	float m_ZDir;
	float m_XDir;
	float m_Timer;
	float m_GlobalTimer;
	float m_TimeSideways;
	float m_TimeForward;
	bool m_FirstLoop;
	bool m_Toggle;
	bool m_CanShoot;
	float m_TimerShoot;
	int m_Random;
	std::wstring m_ModelPath;
	EnemyState m_State;

	GameObject* m_pModelObj;
	ModelComponent* m_pModel;

	void NextState();
	void ChangeState(float dt);
	void IncreaseSpeed();
	void Shoot(float dt);

private:
	EnemyPrefab(const EnemyPrefab& yRef);
	EnemyPrefab& operator=(const EnemyPrefab& yRef);
};

