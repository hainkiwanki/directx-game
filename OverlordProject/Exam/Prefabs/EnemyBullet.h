#pragma once
#include "Scenegraph\GameObject.h"

class EnemyBullet final : public GameObject
{
public:
	EnemyBullet(const XMFLOAT3& startPos, GameObject* source);
	~EnemyBullet(void);

	void Delete() { m_GettingDeleted = true; };
	bool ToBeDeleted() { return m_GettingDeleted; };

protected:
	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);

private:
	bool m_GettingDeleted = false;
	GameObject* m_pSource;
	XMFLOAT3 m_Pos;

	FMOD::Sound* m_pRockHit = nullptr;
	FMOD::Channel* m_pChannel = nullptr;

private:
	EnemyBullet(const EnemyBullet& yRef);
	EnemyBullet& operator=(const EnemyBullet& yRef);
};

