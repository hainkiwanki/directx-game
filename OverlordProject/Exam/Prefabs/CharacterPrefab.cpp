#include "Base\stdafx.h"
#include "CharacterPrefab.h"

#include "Components\Components.h"
#include "..\Overlordproject\Materials\SkinnedDiffuseMaterial.h"
#include "Physx\PhysxManager.h"
#include "..\Overlordproject\Exam\Enums.h"
#include "Scenegraph\GameScene.h"
#include "Components\ModelComponent.h"
#include "Graphics\ModelAnimator.h"
#include "..\Overlordproject\Exam\Manager\BulletManager.h"

CharacterPrefab::CharacterPrefab() :
	m_State{ CharacterState::Idle },
	m_Speed{ 30.0f },
	m_Dir{ 0.0f },
	m_Health{ 3 },
	m_ChangeAnimation{ false },
	m_IsAttacking{ false },
	m_pModel(nullptr)
{}

CharacterPrefab::~CharacterPrefab()
{}

void CharacterPrefab::Reset()
{
	m_Health = 3;
	GetTransform()->Translate(m_OriginalPos);
}

void CharacterPrefab::Initialize(const GameContext & gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	//MODEL
	m_pModel = new ModelComponent(L"Resources/Meshes/Character.ovm");
	m_pModel->SetMaterial(10);
	m_pModelObj = new GameObject();
	m_pModelObj->AddComponent(m_pModel);
	m_pModelObj->SetTag(L"ignore");
	AddChild(m_pModelObj);

	m_pModelObj->GetTransform()->Scale(0.07f, 0.07f, 0.07f);
	m_pModelObj->GetTransform()->Translate(0.0f, -7.0f, 0.0f);

	//PHYSX
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto normalMat = physX->createMaterial(1.0f, 1.0f, 0.0f);

	//CHARACTER CONTROLLER
	m_pCharacterController = new ControllerComponent(normalMat, 3.0f, 10.0f);
	AddComponent(m_pCharacterController);

	//Controls
	gameContext.pInput->AddInputAction(InputAction(0, Down, 'Y'));
	gameContext.pInput->AddInputAction(InputAction(2, Down, VK_LEFT));
	gameContext.pInput->AddInputAction(InputAction(6, Down, XINPUT_GAMEPAD_DPAD_LEFT));

	gameContext.pInput->AddInputAction(InputAction(1, Down, 'T'));
	gameContext.pInput->AddInputAction(InputAction(3, Down, VK_RIGHT));
	gameContext.pInput->AddInputAction(InputAction(7, Down, XINPUT_GAMEPAD_DPAD_RIGHT));

	gameContext.pInput->AddInputAction(InputAction(4, Pressed, VK_SPACE));
	gameContext.pInput->AddInputAction(InputAction(5, Down, XINPUT_GAMEPAD_A));

	SetTag(L"player");

	GetTransform()->Translate(0.0f, 7.0f, 0.0f);
	m_OriginalPos = GetTransform()->GetPosition();
}

void CharacterPrefab::Update(const GameContext & gameContext)
{
	UpdateState(gameContext);
	UpdateAnimation();

	if (m_State != CharacterState::Attack)
	{
		m_pCharacterController->Move(XMFLOAT3(m_Dir * m_Speed * gameContext.pGameTime->GetElapsed(), 0.0f, 0.0f));
	}
}

void CharacterPrefab::SetCharacterState(CharacterState state)
{
	if (m_State == CharacterState::Attack)
	{
		if (!m_pModel->GetAnimator()->IsDone())
		{
			return;
		}
		else
		{
			m_IsAttacking = false;
		}
	}
	if (m_State != state)
	{
		if (state == CharacterState::Attack)
		{
			m_IsAttacking = true;
		}
		m_State = state;
		m_ChangeAnimation = true;
	}
}

void CharacterPrefab::UpdateState(const GameContext& gameContext)
{
	CharacterState state = CharacterState::Idle;
	float dir = 0.0f;
	float rot = 180.0f;

	if (gameContext.pInput->IsActionTriggered(0) ||
		gameContext.pInput->IsActionTriggered(2) ||
		gameContext.pInput->IsActionTriggered(6))
	{
		rot = 90.0f;
		state = CharacterState::Run;
		dir = -1.0f;
	}
	else if (gameContext.pInput->IsActionTriggered(1) ||
			gameContext.pInput->IsActionTriggered(3) ||
			gameContext.pInput->IsActionTriggered(7))
	{
		rot = -90.f;
		state = CharacterState::Run;
		dir = 1.0f;
	}
	else
	{
		rot = 180.0f;
		state = CharacterState::Idle;
		dir = 0.0f;
	}
	if ((gameContext.pInput->IsActionTriggered(4) || 
		gameContext.pInput->IsActionTriggered(5)) && m_IsAttacking == false)
	{
		rot = 180.0f;
		state = CharacterState::Attack;
		auto bulletManager = BulletManager::GetInstance();
		auto posX = GetTransform()->GetPosition().x;
		bulletManager->SpawnBullet(XMFLOAT3(posX, -1.0f, 6.0f), this, GetScene());
	}

	if (m_State != CharacterState::Attack)
	{
		m_pModelObj->GetTransform()->Rotate(0.0f, rot, 0.0f);
		m_Dir = dir;
	}

	SetCharacterState(state);
}

void CharacterPrefab::UpdateAnimation()
{
	auto animator = m_pModel->GetAnimator();
	if (m_ChangeAnimation)
	{
		switch (m_State)
		{
		case CharacterState::Idle:
			animator->SetAnimation(0);
			animator->SetAnimationSpeed(1.0f);
			break;
		case CharacterState::Run:
			animator->SetAnimation(1);
			animator->SetAnimationSpeed(1.0f);
			break;
		case CharacterState::Attack:
			animator->SetAnimation(2);
			animator->SetAnimationSpeed(2.0f);
			break;
		default:
			break;
		}
		m_ChangeAnimation = false;
	}
	if (!animator->IsPlaying())
	{
		animator->Play();
	}
}
