#pragma once
#include "EnemyPrefab.h"

class ZombieGirl final : public EnemyPrefab
{
public:
	ZombieGirl();
	~ZombieGirl(void) = default;

private:
	ZombieGirl(const ZombieGirl& yRef);
	ZombieGirl& operator=(const ZombieGirl& yRef);
};

class ZombieClown final : public EnemyPrefab
{
public:
	ZombieClown();
	~ZombieClown(void) = default;

private:
	ZombieClown(const ZombieClown& yRef);
	ZombieClown& operator=(const ZombieClown& yRef);
};

class ZombiePrison final : public EnemyPrefab
{
public:
	ZombiePrison();
	~ZombiePrison(void) = default;

private:
	ZombiePrison(const ZombiePrison& yRef);
	ZombiePrison& operator=(const ZombiePrison& yRef);
};

