#include "Base\stdafx.h"
#include "ZombiePrefab.h"
#include "EnemyPrefab.h"

ZombieGirl::ZombieGirl() :
	EnemyPrefab(41, L"Resources/Meshes/ZombieGirl.ovm", 1)
{
	m_Score = 100;
}

ZombieClown::ZombieClown() :
	EnemyPrefab(42, L"Resources/Meshes/ZombieClown.ovm", 2)
{
	m_Score = 200;
}

ZombiePrison::ZombiePrison() :
	EnemyPrefab(40, L"Resources/Meshes/ZombieClown.ovm", 3)
{
	m_Score = 400;
}
