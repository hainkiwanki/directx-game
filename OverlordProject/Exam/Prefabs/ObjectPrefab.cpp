#include "Base\stdafx.h"
#include "ObjectPrefab.h"
#include <Physx/PhysxProxy.h>
#include "Physx\PhysxManager.h"
#include "Components\Components.h"

void FloorPanel::Initialize(const GameContext & gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	auto pModelObject = new GameObject();
	auto pModel = new ModelComponent(L"Resources/Meshes/FloorPanel.ovm");
	pModel->SetMaterial(31);
	pModelObject->AddComponent(pModel);
	AddChild(pModelObject);

	pModelObject->GetTransform()->Rotate(90.0f, 0.0f, 0.0f);
	pModelObject->GetTransform()->Scale(7.0f, 7.0f, 7.0f);
}

void WallPanel::Initialize(const GameContext & gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	auto pModelObject = new GameObject();
	auto pModel = new ModelComponent(L"Resources/Meshes/WallPanel.ovm");
	if(m_WithText)
		pModel->SetMaterial(33);
	else
		pModel->SetMaterial(32);
	pModelObject->AddComponent(pModel);
	AddChild(pModelObject);

	if (m_OnRight)
		pModelObject->GetTransform()->Rotate(0.0f, 90.0f, 0.0f);
	else
		pModelObject->GetTransform()->Rotate(0.0f, -90.0f, 0.0f);
	pModelObject->GetTransform()->Scale(10.0f, 10.0f, 10.0f);

	auto pModelObject2 = new GameObject();
	auto pModel2 = new ModelComponent(L"Resources/Meshes/WallPanel.ovm");
	pModel2->SetMaterial(21);
	pModelObject2->AddComponent(pModel2);
	AddChild(pModelObject2);

	pModelObject2->GetTransform()->Rotate(90.0f, 90.0f, 0.0f);
	pModelObject2->GetTransform()->Scale(10.0f, 25.0f, 1.0f);
	if(m_OnRight)
		pModelObject2->GetTransform()->Translate(25.0f, 10.0f, 0.0f);
	else
		pModelObject2->GetTransform()->Translate(-25.0f, 10.0f, 0.0f);

	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto material = physX->createMaterial(0.5f, 0.5f, 0.0f);

	SetTag(L"wall");
	auto pPanelRigidBody = new RigidBodyComponent(true);
	AddComponent(pPanelRigidBody);
	std::shared_ptr<PxGeometry> geom(new PxBoxGeometry(1.0f, 10.0f, 10.0f));
	AddComponent(new ColliderComponent(geom, *material, PxTransform(PxIdentity)));

	GetTransform()->Translate(m_Offset.x, m_Offset.y + 10.0f, m_Offset.z);
}
