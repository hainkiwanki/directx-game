#include "Base\stdafx.h"
#include "BulletPrefab.h"
#include "Components\Components.h"
#include "..\Overlordproject\Materials\SkinnedDiffuseMaterial.h"
#include "Physx\PhysxManager.h"
#include "..\Overlordproject\Exam\Enums.h"
#include "Scenegraph\GameScene.h"
#include "Prefabs\Prefabs.h"
#include "..\Overlordproject\Exam\smallMaths.h"
#include "Base\SoundManager.h"
#include "..\Overlordproject\Exam\Prefabs\CustomPrefabs.h"

BulletPrefab::BulletPrefab(const XMFLOAT3& startPos, GameObject* source) :
	m_Pos{ startPos }, m_pSource{ source }
{}

BulletPrefab::~BulletPrefab(){}

void BulletPrefab::Initialize(const GameContext & gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	//PHYSX
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto normalMat = physX->createMaterial(1.0f, 1.0f, 0.0f);
	auto fmodSystem = SoundManager::GetInstance()->GetSystem();

	fmodSystem->createSound("Resources/Sounds/StoneSpawn.wav", (FMOD_2D | FMOD_DEFAULT), NULL, &m_pRockSpawn);
	fmodSystem->createSound("Resources/Sounds/StonePunch.wav", (FMOD_2D | FMOD_DEFAULT), NULL, &m_pRockPunch);
	fmodSystem->createSound("Resources/Sounds/StoneHit.wav", (FMOD_2D | FMOD_DEFAULT), NULL, &m_pRockHit);
	fmodSystem->playSound(m_pRockSpawn, 0, true, &m_pChannel);
	m_pChannel->setVolume(1.0f);
	m_pChannel->setPaused(false);

	//MESH
	auto pRockObj = new GameObject();
	auto pModel = new ModelComponent(L"Resources/Meshes/Rock.ovm");
	int random = rand() % 100;
	if (random < 50)
		pModel->SetMaterial(34);
	else
		pModel->SetMaterial(35);
	pRockObj->AddComponent(pModel);
	pRockObj->GetTransform()->Scale(2.0f, 2.0f, 2.0f);
	pRockObj->GetTransform()->Translate(0.5f, 0.0f, 0.0f);
	AddChild(pRockObj);

	//RIGIDBODY
	auto pRigidBody = new RigidBodyComponent();
	pRigidBody->SetKinematic(true);
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group1);
	pRigidBody->SetCollisionIgnoreGroups(static_cast<CollisionGroupFlag>(Group2));
	AddComponent(pRigidBody);
	
	//COLLIDER
	std::shared_ptr<PxGeometry> sphereGeom(new PxSphereGeometry(1.0f));
	auto pCollider = new ColliderComponent(sphereGeom, *normalMat, PxTransform(PxIdentity));
	pCollider->EnableTrigger(true);
	AddComponent(pCollider);
	SetTag(L"bullet");

	//TRIGGER CALLBACK LAMBDA
	auto lambda = [=](GameObject * triggerobject, GameObject * otherobject, GameObject::TriggerAction action)
	{
		UNREFERENCED_PARAMETER(triggerobject);
		if (action == GameObject::TriggerAction::ENTER)
		{
			if (otherobject != m_pSource)
			{
				if (otherobject->GetTag() == L"enemy")
				{
					auto enemy = static_cast<EnemyPrefab*>(otherobject);
					enemy->DecreaseHealth();
					fmodSystem->playSound(m_pRockHit, 0, true, &m_pChannel);
					m_pChannel->setVolume(1.0f);
					m_pChannel->setPaused(false);
				}
				Delete();
			}
		}
	};
	PhysicsCallback f_test = lambda;
	SetOnTriggerCallBack(f_test);

	GetTransform()->Translate(m_Pos.x, m_Pos.y, m_Pos.z);
}

void BulletPrefab::Update(const GameContext & gameContext)
{
	auto pos = GetTransform()->GetPosition();
	auto dt = gameContext.pGameTime->GetElapsed();
	if (!m_IsShot)
	{
		float maxtime = 0.45f;
		pos.y = smoll::Lerp(m_BeginPos, m_EndPos, m_Timer / maxtime);
		m_Timer += dt;
		if (m_Timer >= maxtime)
		{
			m_IsShot = true;
			auto fmodSystem = SoundManager::GetInstance()->GetSystem();
			fmodSystem->playSound(m_pRockPunch, 0, true, &m_pChannel);
			m_pChannel->setVolume(0.7f);
			m_pChannel->setPaused(false);
		}
	}
	else
	{
		pos.z += 60.0f * dt;
	}
	GetTransform()->Translate(pos.x, pos.y, pos.z);
}