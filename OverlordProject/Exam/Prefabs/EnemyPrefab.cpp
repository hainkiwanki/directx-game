#include "Base\stdafx.h"
#include "EnemyPrefab.h"

#include "Components\Components.h"
#include "..\Overlordproject\Materials\SkinnedDiffuseMaterial.h"
#include "Physx\PhysxManager.h"
#include "Scenegraph\GameScene.h"
#include "Components\ModelComponent.h"
#include "Graphics\ModelAnimator.h"
#include "..\Overlordproject\Exam\Manager\BulletManager.h"
#include "..\Overlordproject\Exam\Enums.h"

EnemyPrefab::EnemyPrefab(int matId, const std::wstring& model, int health) :
	m_ModelPath{ model },
	m_Health{ health },
	m_Speed{ 5.0f },
	m_NewSpeed{ 5.0f },
	m_ZDir{ 0.0f },
	m_XDir{ 0.0f },
	m_pModel(nullptr),
	m_pModelObj(nullptr),
	m_MatId(matId),
	m_Timer(0.0f),
	m_GlobalTimer{ 0.0f },
	m_State{ EnemyState::Left },
	m_FirstLoop{ true },
	m_Toggle{ false },
	m_TimeForward{ 1.0f },
	m_TimeSideways{ 6.0f },
	m_CanShoot{false},
	m_TimerShoot{0.0f},
	m_Random{0},
	m_ShootChance{60}
{
	m_TimerShoot = float(rand() % 4);
}

EnemyPrefab::~EnemyPrefab()
{}

void EnemyPrefab::Initialize(const GameContext & gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	//MODEL
	m_pModel = new ModelComponent(m_ModelPath);
	m_pModel->SetMaterial(m_MatId);
	m_pModelObj = new GameObject();
	m_pModelObj->AddComponent(m_pModel);
	m_pModelObj->SetTag(L"ignore");
	AddChild(m_pModelObj);

	m_pModelObj->GetTransform()->Scale(0.07f, 0.07f, 0.07f);
	m_pModelObj->GetTransform()->Translate(0.0f, -7.0f, 0.0f);

	//PHYSX
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto normalMat = physX->createMaterial(1.0f, 1.0f, 0.0f);

	//RIGIDBODY
	auto pRigidBody = new RigidBodyComponent();
	pRigidBody->SetKinematic(true);
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group1);
	AddComponent(pRigidBody);

	//COLLIDER
	std::shared_ptr<PxGeometry> geom(new PxBoxGeometry(2.0f, 6.0f, 2.0f));
	auto pCollider = new ColliderComponent(geom, *normalMat, PxTransform(PxIdentity));
	AddComponent(pCollider);

	SetTag(L"enemy");
}

void EnemyPrefab::Update(const GameContext & gameContext)
{
	auto animator = m_pModel->GetAnimator();
	if(!animator->IsPlaying())
		animator->Play();
	float dt = gameContext.pGameTime->GetElapsed();

	Shoot(dt);

	auto pos = GetTransform()->GetPosition();
	ChangeState(dt);
	pos.x += m_XDir * m_Speed * dt;
	pos.z += m_ZDir * m_Speed * dt;
	GetTransform()->Translate(pos);
}

void EnemyPrefab::NextState()
{
	switch (m_State)
	{
	case EnemyState::Left:
		m_State = EnemyState::Forward;
		break;
	case EnemyState::Forward:
		if(m_Toggle)
			m_State = EnemyState::Left;
		else
			m_State = EnemyState::Right;
		break;
	case EnemyState::Right:
		m_State = EnemyState::Forward;
		IncreaseSpeed();
		break;
	default:
		break;
	}
}

void EnemyPrefab::ChangeState(float dt)
{
	m_Timer += dt;
	switch (m_State)
	{
	case EnemyState::Left:
		m_XDir = -1.0f;
		m_ZDir = 0.0f;
		break;
	case EnemyState::Forward:
		m_XDir = 0.0f;
		m_ZDir = -1.0f;
		break;
	case EnemyState::Right:
		m_XDir = 1.0f;
		m_ZDir = 0.0f;
		break;
	default:
		break;
	}
	if (m_State != EnemyState::Forward && !m_FirstLoop)
	{
		if (m_Timer >= m_TimeSideways)
		{
			m_Timer = 0.0f;
			NextState();
		}
	}
	else
	{
		if (m_FirstLoop)
		{
			if (m_Timer >= (m_TimeSideways / 2.0f))
			{
				m_Toggle = !m_Toggle;
				m_FirstLoop = false;
				m_Timer = 0.0f;
				NextState();
			}
		}
		else
		{
			if (m_Timer >= m_TimeForward)
			{
				m_Toggle = !m_Toggle;
				m_Timer = 0.0f;
				NextState();
			}
		}
	}
}

void EnemyPrefab::IncreaseSpeed()
{
	m_NewSpeed += 0.7f;
	float scale = m_Speed / m_NewSpeed;
	m_Speed = m_NewSpeed;
	m_TimeForward *= scale;
	m_TimeSideways *= scale;
}

void EnemyPrefab::Shoot(float dt)
{
	auto pos = GetTransform()->GetPosition();
	if (m_CanShoot)
	{
		if (m_Random > 90)
		{
			auto pos = GetTransform()->GetPosition();
			BulletManager::GetInstance()->SpawnEnemyBullet(XMFLOAT3(pos.x, 7.0f, pos.z), this, GetScene());
		}
		m_CanShoot = false;
	}
	else if (!m_CanShoot)
	{
		m_TimerShoot += dt;
		if (m_TimerShoot >= 5.0f)
		{
			m_Random = rand() % 100;
			m_CanShoot = true;
			m_TimerShoot = 0.0f;
		}
	}
}
