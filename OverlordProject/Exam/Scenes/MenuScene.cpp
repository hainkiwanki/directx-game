//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "MenuScene.h"
#include <Components\Components.h>
#include <Physx\PhysxProxy.h>
#include "Graphics\TextureData.h"
#include "Components\SpriteComponent.h"
#include "Scenegraph\SceneManager.h"
#include "Scenegraph\GameObject.h"
#include "Base\SoundManager.h"

MenuScene::MenuScene() :
	GameScene(L"MenuScene"),
	m_Selection{ Selection::Start }{}

MenuScene::~MenuScene(){}

void MenuScene::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pCursorObj = new GameObject();
	m_pCursorObj->AddComponent(new SpriteComponent(L"Resources/Textures/Cursor.png", XMFLOAT2(0.5f, 0.5f), XMFLOAT4(1, 1, 1, 1.5f)));
	m_pCursorObj2 = new GameObject();
	m_pCursorObj2->AddComponent(new SpriteComponent(L"Resources/Textures/Cursor.png", XMFLOAT2(0.5f, 0.5f), XMFLOAT4(1, 1, 1, 1.5f)));
	AddChild(m_pCursorObj);
	AddChild(m_pCursorObj2);

	m_pMenuObj = new GameObject();
	m_pMenuObj->AddComponent(new SpriteComponent(L"Resources/Textures/MainMenu.png", XMFLOAT2(0.0f, 0.0f), XMFLOAT4(1, 1, 1, 1.5f)));
	AddChild(m_pMenuObj);

	//Input
	gameContext.pInput->AddInputAction(InputAction(0, Pressed, VK_UP));
	gameContext.pInput->AddInputAction(InputAction(1, Pressed, VK_DOWN));
	gameContext.pInput->AddInputAction(InputAction(2, Pressed, VK_RETURN));
	gameContext.pInput->AddInputAction(InputAction(3, Pressed, VK_SPACE));
	gameContext.pInput->AddInputAction(InputAction(4, Down, XINPUT_GAMEPAD_A));

	//Sound
	auto fmodSystem = SoundManager::GetInstance()->GetSystem();
	fmodSystem->createSound("Resources/Sounds/Theme.mp3", (FMOD_2D | FMOD_LOOP_NORMAL), NULL, &m_pThemeSong);
	fmodSystem->createSound("Resources/Sounds/InterfaceChange.wav", (FMOD_2D | FMOD_DEFAULT), NULL, &m_pInterfaceChange);
	fmodSystem->playSound(m_pThemeSong, 0, true, &m_pMusicChannel);
	m_pMusicChannel->setVolume(5.0f);
}

void MenuScene::Update(const GameContext& gameContext)
{
	UpdateCursors();


	if (gameContext.pInput->IsActionTriggered(0) || gameContext.pInput->IsActionTriggered(1))
	{
		auto fmodSystem = SoundManager::GetInstance()->GetSystem();
		fmodSystem->playSound(m_pInterfaceChange, 0, true, &m_pFxChannel);
		m_pFxChannel->setVolume(0.3f);
		m_pFxChannel->setPaused(false);
		SwitchState();
	}
	else if (gameContext.pInput->IsActionTriggered(2) ||
		gameContext.pInput->IsActionTriggered(3) || gameContext.pInput->IsActionTriggered(4))
	{
		switch (m_Selection)
		{
		case Selection::Start:
			SceneManager::GetInstance()->SetActiveGameScene(L"SpaceScene");
			break;
		case Selection::Exit:
			PostQuitMessage(0);
			break;
		default:
			break;
		}
	}
}

void MenuScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void MenuScene::SceneActivated()
{
	m_pMusicChannel->setPaused(false);
}

void MenuScene::SceneDeactivated()
{
	m_pMusicChannel->setPaused(true);
}

void MenuScene::UpdateCursors()
{
	switch (m_Selection)
	{
	case Selection::Start:
		m_Cursor1Pos = { 558.0f, 540.0f, 0.0f };
		m_Cursor2Pos = { 727.0f, 540.0f, 0.0f };
		break;
	case Selection::Exit:
		m_Cursor1Pos = { 572.0f, 580.0f, 0.0f };
		m_Cursor2Pos = { 715.0f, 580.0f, 0.0f };
		break;
	default:
		break;
	}

	m_pCursorObj->GetTransform()->Translate(m_Cursor1Pos.x, m_Cursor1Pos.y, m_Cursor1Pos.z);
	m_pCursorObj2->GetTransform()->Translate(m_Cursor2Pos.x, m_Cursor2Pos.y, m_Cursor2Pos.z);
}

void MenuScene::SwitchState()
{
	switch (m_Selection)
	{
	case Selection::Start:
		m_Selection = Selection::Exit;
		break;
	case Selection::Exit:
		m_Selection = Selection::Start;
		break;
	default:
		break;
	}
}