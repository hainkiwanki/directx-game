#pragma once
#include <Scenegraph/GameScene.h>

class TextureData;

enum class Selection
{
	Start,
	Exit
};

class MenuScene: public GameScene
{
public:
	MenuScene();
	~MenuScene();

	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;
	void SceneActivated() override;
	void SceneDeactivated() override;

private:
	GameObject* m_pCursorObj = nullptr;
	GameObject* m_pCursorObj2 = nullptr;
	GameObject* m_pMenuObj = nullptr;

	Selection m_Selection;
	PxVec3 m_Cursor1Pos;
	PxVec3 m_Cursor2Pos;

	FMOD::Sound* m_pThemeSong = nullptr;
	FMOD::Sound* m_pInterfaceChange = nullptr;
	FMOD::Channel* m_pMusicChannel = nullptr;
	FMOD::Channel* m_pFxChannel = nullptr;

	void UpdateCursors();
	void SwitchState();
private:
	MenuScene(const MenuScene &obj) = delete;
	MenuScene& operator=(const MenuScene& obj) = delete;
};

