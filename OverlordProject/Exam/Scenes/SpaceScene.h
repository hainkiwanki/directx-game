#pragma once
#include <Scenegraph/GameScene.h>

class CameraComponent;
class CharacterPrefab;
class PostCustom;
class MenuPrefab;
enum class GameState
{
	Play,
	Pause,
	Controls,
	Win,
	Lose
};
enum class Select
{
	MainMenu,
	Continue,
	Restart,
	Controls,
	Back,
	Quit
};

class SpaceScene: public GameScene
{
public:
	SpaceScene();
	~SpaceScene();

	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;
	void SceneActivated() override;
	void SceneDeactivated() override;

private:
	CameraComponent* m_pCamera = nullptr;
	CharacterPrefab* m_pCharacter = nullptr;
	GameObject* m_pSphere = nullptr;
	GameObject* m_pSkyBox = nullptr;	
	PostCustom* m_PPcustum = nullptr;
	float m_Contrast = 0.0f;
	bool m_Bloom = false;
	GameState m_State = GameState::Play;
	Select m_Select = Select::MainMenu;
	MenuPrefab* m_pPauseMenu;
	MenuPrefab* m_pControlMenu;
	MenuPrefab* m_pWin;
	MenuPrefab* m_pLose;
	bool m_Lost = false;

	FMOD::Sound* m_pMusic = nullptr;
	FMOD::Sound* m_pInterface = nullptr;
	FMOD::Channel* m_pChannel = nullptr;
	FMOD::Channel* m_pChannelMusic = nullptr;

	void HandleInput(const GameContext& gameContext);
	void HandleGameInput(const GameContext& gameContext);
	void HandleMenuInput(const GameContext& gameContext);
	void HandleWinLoseInput(const GameContext& gameContext);
	void UpdateManager();
	void TogglePauseEverything();
	void NextSelectionState();
	void PreviousSelectionState();
	void MenuAction();
	void ResetScene();
	void CheckLossWinConditions();
private:
	SpaceScene(const SpaceScene &obj) = delete;
	SpaceScene& operator=(const SpaceScene& obj) = delete;
};

