//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "SpaceScene.h"
#include <Components/Components.h>
#include <Physx\PhysxProxy.h>
#include "Prefabs\FixedCamera.h"
#include "Prefabs\Prefabs.h"
#include "..\Overlordproject\Exam\Prefabs\CustomPrefabs.h"
#include "Components\ParticleEmitterComponent.h"
#include "..\Overlordproject\Exam\Manager\BulletManager.h"
#include "..\Overlordproject\Exam\Manager\TextureLoader.h"
#include "..\Overlordproject\Exam\Manager\LevelManager.h"
#include "..\Overlordproject\Exam\Manager\InterfaceManager.h"
#include "Graphics\PostProcessingMaterial.h"
#include "..\Overlordproject\Materials\Post\PostProcessors.h"
#include "Scenegraph\SceneManager.h"
#include "Base\SoundManager.h"
#include "Physx\PhysxManager.h"

SpaceScene::SpaceScene():
	GameScene(L"SpaceScene")
{}


SpaceScene::~SpaceScene()
{
	BulletManager::DestroyInstance();
	TextureLoader::DestroyInstance();
	LevelManager::DestroyInstance();
	InterfaceManager::DestroyInstance();
}

void SpaceScene::Initialize(const GameContext& gameContext)
{

	auto pObj = new GameObject();
	auto m_pParticleEmitter = new ParticleEmitterComponent(L"./Resources/Textures/Fire.png", 160);
	m_pParticleEmitter->SetVelocity(XMFLOAT3(3.0, 0.0f, 0));
	m_pParticleEmitter->SetMinSize(1.0f);
	m_pParticleEmitter->SetMaxSize(2.0f);
	m_pParticleEmitter->SetMinEnergy(10.0f);
	m_pParticleEmitter->SetMaxEnergy(30.0f);
	m_pParticleEmitter->SetMinSizeGrow(1.0f);
	m_pParticleEmitter->SetMaxSizeGrow(1.0f);
	m_pParticleEmitter->SetMinEmitterRange(1.0f);
	m_pParticleEmitter->SetMaxEmitterRange(10.0f);
	m_pParticleEmitter->SetColor(XMFLOAT4(1.0f, 1.0f, 1.0f, 0.6f));
	pObj->AddComponent(m_pParticleEmitter);
	AddChild(pObj);
	pObj->GetTransform()->Translate(-25.0f, 50.0f, 0.0f);


	//RIGIDBODY
	auto pObj2 = new GameObject();
	auto pRigidBody = new RigidBodyComponent();
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group1);
	pRigidBody->SetKinematic(true);
	pObj2->AddComponent(pRigidBody);

	//COLLIDER
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto normalMat = physX->createMaterial(1.0f, 1.0f, 0.0f);
	std::shared_ptr<PxGeometry> boxGeom(new PxBoxGeometry(50.0f, 10.0f, 1.0f));
	auto pCollider = new ColliderComponent(boxGeom, *normalMat, PxTransform(PxIdentity));
	pCollider->EnableTrigger(true);
	pObj2->AddComponent(pCollider);

	auto lambda = [=](GameObject * triggerobject, GameObject * otherobject, GameObject::TriggerAction action)
	{
		UNREFERENCED_PARAMETER(triggerobject);
		if (action == GameObject::TriggerAction::ENTER)
		{
			if (otherobject->GetTag() == L"enemy")
			{
				m_Lost = true;
			}
		}
	};
	GameObject::PhysicsCallback f_test = lambda;
	pObj2->SetOnTriggerCallBack(f_test);
	AddChild(pObj2);


	//CAMERA
	auto camera = new FixedCamera();
	m_pCamera = new CameraComponent();
	camera->AddComponent(m_pCamera);
	camera->GetTransform()->Translate(0, 70.0f, -42.0f);
	camera->GetTransform()->Rotate(45.0f, 0.0f, 0.0f);
	AddChild(camera);
	SetActiveCamera(m_pCamera);

	auto fmodSystem = SoundManager::GetInstance()->GetSystem();
	fmodSystem->createSound("Resources/Sounds/SpaceTheme.mp3", (FMOD_2D | FMOD_LOOP_NORMAL), NULL, &m_pMusic);
	fmodSystem->createSound("Resources/Sounds/InterfaceChange.wav", (FMOD_2D | FMOD_DEFAULT), NULL, &m_pInterface);
	fmodSystem->playSound(m_pMusic, NULL, true, &m_pChannelMusic);
	m_pChannelMusic->setVolume(0.5f);

	//DEBUG RENDERING
	GetPhysxProxy()->EnablePhysxDebugRendering(true);
	auto levelMan = LevelManager::GetInstance();
	auto textureMan = TextureLoader::GetInstance();
	auto interfaceMan = InterfaceManager::GetInstance();

	textureMan->Initialize(gameContext);
	m_pCharacter = new CharacterPrefab();
	AddChild(m_pCharacter);
	interfaceMan->Initialize(m_pCharacter, this);
	levelMan->CreateLevel(this);
	levelMan->CreateEnemies(this);

	m_PPcustum = new PostCustom();
	gameContext.pMaterialManager->AddMaterial_PP(m_PPcustum, 0);
	auto bloom = new PostBloom();
	gameContext.pMaterialManager->AddMaterial_PP(bloom, 1);
	AddPostProcessingMaterial(0);

	m_pPauseMenu = new MenuPrefab(L"Resources/Textures/GamePause.png");
	m_pPauseMenu->AddCursorPosition(XMFLOAT3(472.0f, 193.0f, 0.0f));
	m_pPauseMenu->AddCursorPosition(XMFLOAT3(741.0f, 193.0f, 0.0f));
	m_pPauseMenu->AddCursorPosition(XMFLOAT3(486.0f, 249.0f, 0.0f));
	m_pPauseMenu->AddCursorPosition(XMFLOAT3(729.0f, 249.0f, 0.0f));
	m_pPauseMenu->AddCursorPosition(XMFLOAT3(499.0f, 316.0f, 0.0f));
	m_pPauseMenu->AddCursorPosition(XMFLOAT3(717.0f, 316.0f, 0.0f));
	m_pPauseMenu->AddCursorPosition(XMFLOAT3(489.0f, 374.0f, 0.0f));
	m_pPauseMenu->AddCursorPosition(XMFLOAT3(730.0f, 374.0f, 0.0f));
	m_pPauseMenu->AddCursorPosition(XMFLOAT3(539.0f, 428.0f, 0.0f));
	m_pPauseMenu->AddCursorPosition(XMFLOAT3(673.0f, 428.0f, 0.0f));
	AddChild(m_pPauseMenu);

	m_pControlMenu = new MenuPrefab(L"Resources/Textures/Controls.png");
	m_pControlMenu->AddCursorPosition(XMFLOAT3(567.0f, 669.0f, 0.0f));
	m_pControlMenu->AddCursorPosition(XMFLOAT3(702.0f, 669.0f, 0.0f));
	AddChild(m_pControlMenu);

	m_pWin = new MenuPrefab(L"Resources/Textures/WinScreen.png");
	AddChild(m_pWin);

	m_pLose = new MenuPrefab(L"Resources/Textures/LoseScreen.png");
	AddChild(m_pLose);

	//INPUT
	gameContext.pInput->AddInputAction(InputAction(10, Pressed, VK_ESCAPE));
	gameContext.pInput->AddInputAction(InputAction(11, Pressed, VK_NUMPAD1));
	gameContext.pInput->AddInputAction(InputAction(12, Pressed, VK_NUMPAD2));
	gameContext.pInput->AddInputAction(InputAction(13, Pressed, VK_NUMPAD3));
	gameContext.pInput->AddInputAction(InputAction(14, Pressed, VK_NUMPAD4));

	gameContext.pInput->AddInputAction(InputAction(21, Pressed, VK_UP));
	gameContext.pInput->AddInputAction(InputAction(22, Pressed, VK_DOWN));
	gameContext.pInput->AddInputAction(InputAction(23, Pressed, VK_RETURN));
	gameContext.pInput->AddInputAction(InputAction(24, Pressed, VK_SPACE));
}

void SpaceScene::Update(const GameContext& gameContext)
{
	CheckLossWinConditions();
	HandleInput(gameContext);
	if (m_State == GameState::Play)
	{
		UpdateManager();
	}
}

void SpaceScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void SpaceScene::SceneActivated()
{
	m_pChannelMusic->setPaused(false);
}

void SpaceScene::SceneDeactivated()
{
	m_pChannelMusic->setPaused(true);
}

void SpaceScene::HandleInput(const GameContext & gameContext)
{
	switch (m_State)
	{
	case GameState::Play:
		HandleGameInput(gameContext);
		break;
	case GameState::Pause:
	case GameState::Controls:
		HandleMenuInput(gameContext);
		break;
	case GameState::Win:
	case GameState::Lose:
		HandleWinLoseInput(gameContext);
		break;
	default:
		break;
	}
}

void SpaceScene::HandleGameInput(const GameContext & gameContext)
{
	if (gameContext.pInput->IsActionTriggered(10))
	{
		TogglePauseEverything();
		m_State = GameState::Pause;
		m_pPauseMenu->Unhide();
	}
	if (gameContext.pInput->IsActionTriggered(11))
	{
		m_Contrast += 0.1f;
		if (m_Contrast > 2.5f)
			m_Contrast = 0.0f;
		m_PPcustum->SetContrast(m_Contrast);
	}
	if (gameContext.pInput->IsActionTriggered(12))
	{
		m_Bloom = !m_Bloom;
		if (m_Bloom)
			AddPostProcessingMaterial(1);
		else
			RemovePostProcessingMaterial(1);
	}
	if (gameContext.pInput->IsActionTriggered(13))
	{
		m_PPcustum->Toggle4Bit();
	}
	if (gameContext.pInput->IsActionTriggered(14))
	{
		m_PPcustum->ToggleInverse();
	}
}

void SpaceScene::HandleMenuInput(const GameContext & gameContext)
{
	auto fmodSystem = SoundManager::GetInstance()->GetSystem();
	if (gameContext.pInput->IsActionTriggered(10))
	{
		if (m_Select != Select::Back)
		{
			TogglePauseEverything();
			m_State = GameState::Play;
			m_pPauseMenu->Hide();
		}
	}
	if (gameContext.pInput->IsActionTriggered(21))
	{
		m_pPauseMenu->PreviousPosition();
		PreviousSelectionState();
		fmodSystem->playSound(m_pInterface, 0, true, &m_pChannel);
		m_pChannel->setVolume(0.3f);
		m_pChannel->setPaused(false);
	}
	else if (gameContext.pInput->IsActionTriggered(22))
	{
		m_pPauseMenu->NextPosition();
		NextSelectionState();
		fmodSystem->playSound(m_pInterface, 0, true, &m_pChannel);
		m_pChannel->setVolume(0.3f);
		m_pChannel->setPaused(false);
	}
	if (gameContext.pInput->IsActionTriggered(23) || gameContext.pInput->IsActionTriggered(24))
	{
		MenuAction();
	}
}

void SpaceScene::HandleWinLoseInput(const GameContext & gameContext)
{
	if (gameContext.pInput->IsActionTriggered(10))
	{
		//restart
		ResetScene();
	}
	if (gameContext.pInput->IsActionTriggered(24))
	{
		//to main menu	
		ResetScene();
		SceneManager::GetInstance()->SetActiveGameScene(L"MenuScene");
	}
}

void SpaceScene::UpdateManager()
{
	LevelManager::GetInstance()->Update();
	InterfaceManager::GetInstance()->Update();
	BulletManager::GetInstance()->Update();
}

void SpaceScene::TogglePauseEverything()
{
	for (auto g : GetChildren())
	{
		if(g->GetTag() != L"UI")
			g->ToggleActive();
	}
}

void SpaceScene::NextSelectionState()
{
	switch (m_Select)
	{
	case Select::MainMenu:
		m_Select = Select::Continue;
		break;
	case Select::Continue:
		m_Select = Select::Restart;
		break;
	case Select::Restart:
		m_Select = Select::Controls;
		break;
	case Select::Controls:
		m_Select = Select::Quit;
		break;
	case Select::Quit:
		break;
	default:
		break;
	}
}

void SpaceScene::PreviousSelectionState()
{
	switch (m_Select)
	{
	case Select::MainMenu:
		break;
	case Select::Continue:
		m_Select = Select::MainMenu;
		break;
	case Select::Restart:
		m_Select = Select::Continue;
		break;
	case Select::Controls:
		m_Select = Select::Restart;
		break;
	case Select::Quit:
		m_Select = Select::Controls;
		break;
	default:
		break;
	}
}

void SpaceScene::MenuAction()
{
	switch (m_Select)
	{
	case Select::MainMenu:
		ResetScene();
		SceneManager::GetInstance()->SetActiveGameScene(L"MenuScene");
		break;
	case Select::Continue:
		TogglePauseEverything();
		m_State = GameState::Play;
		m_pPauseMenu->Hide();
		break;
	case Select::Restart:
		ResetScene();
		break;
	case Select::Controls:
		m_pPauseMenu->Hide();
		m_Select = Select::Back;
		m_pControlMenu->Unhide();
		break;
	case Select::Back:
		m_pPauseMenu->Unhide();
		m_Select = Select::Controls;
		m_pControlMenu->Hide();
		break;
	case Select::Quit:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
}

void SpaceScene::ResetScene()
{
	m_State = GameState::Play;
	m_pControlMenu->Hide();
	m_pPauseMenu->PreviousPosition();
	PreviousSelectionState();
	m_pPauseMenu->Hide();
	TogglePauseEverything();
	m_pCharacter->Reset();
	m_pWin->Hide();
	m_pLose->Hide();
	BulletManager::GetInstance()->Reset();
	LevelManager::GetInstance()->Reset(this);
	InterfaceManager::GetInstance()->Reset();
}

void SpaceScene::CheckLossWinConditions()
{
	if (m_State != GameState::Lose && m_State != GameState::Win)
	{
		if (!m_pCharacter->IsAlive() || m_Lost)
		{
			m_Lost = false;
			m_State = GameState::Lose;
			m_pLose->Unhide();
			TogglePauseEverything();
		}
		else if (LevelManager::GetInstance()->EnemiesLeft() == 0)
		{
			m_State = GameState::Win;
			m_pWin->Unhide();
			TogglePauseEverything();
		}
	}
}