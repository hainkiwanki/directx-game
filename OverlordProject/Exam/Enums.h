#pragma once
enum class CharacterState
{
	Idle,
	Run,
	Attack
};

enum class EnemyState
{
	Left,
	Forward,
	Right
};