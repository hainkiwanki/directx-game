//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "TextureLoader.h"
#include "Scenegraph\GameScene.h"
#include "..\Overlordproject\Materials\SkinnedDiffuseMaterial.h"
#include "..\Overlordproject\Materials\DiffuseMaterial.h"
#include "..\Overlordproject\Materials\ColorMaterial.h"
#include "..\Overlordproject\Materials\ModelMaterial.h"

TextureLoader::TextureLoader(void)
{}

TextureLoader::~TextureLoader(void)
{}

void TextureLoader::Initialize(const GameContext& gameContext)
{
	//SKINNED MATERIAL
	auto zombieGirlMat = new SkinnedDiffuseMaterial();
	zombieGirlMat->SetDiffuseTexture(L"Resources/Textures/ZombieGirl.png");
	gameContext.pMaterialManager->AddMaterial(zombieGirlMat, 41);

	auto zombieClownMat = new SkinnedDiffuseMaterial();
	zombieClownMat->SetDiffuseTexture(L"Resources/Textures/ZombieClown.png");
	gameContext.pMaterialManager->AddMaterial(zombieClownMat, 42);

	auto zombiePrisonMat = new SkinnedDiffuseMaterial();
	zombiePrisonMat->SetDiffuseTexture(L"Resources/Textures/ZombiePrison.png");
	gameContext.pMaterialManager->AddMaterial(zombiePrisonMat, 40);

	auto characterMat = new SkinnedDiffuseMaterial();
	characterMat->SetDiffuseTexture(L"Resources/Textures/Ninja.png");
	gameContext.pMaterialManager->AddMaterial(characterMat, 10);
	
	XMFLOAT3 lightDir = XMFLOAT3(0.1f, -0.4f, 0.2f);

	auto floorPanelMat = new ModelMaterial();
	floorPanelMat->SetDiffuseTexture(L"Resources/Textures/FloorPanel.png");
	floorPanelMat->SetNormalTexture(L"Resources/Textures/FloorPanel_Normal.png");
	floorPanelMat->SetLightDirection(lightDir);
	gameContext.pMaterialManager->AddMaterial(floorPanelMat, 31);

	auto wallPanelMat = new ModelMaterial();
	wallPanelMat->SetDiffuseTexture(L"Resources/Textures/WallPanelWithoutText.png");
	wallPanelMat->SetNormalTexture(L"Resources/Textures/Panel_Normal.png");
	wallPanelMat->SetLightDirection(lightDir);
	gameContext.pMaterialManager->AddMaterial(wallPanelMat, 32);

	auto wallPanelMatWithText = new ModelMaterial();
	wallPanelMatWithText->SetDiffuseTexture(L"Resources/Textures/WallPanelWIthText.png");
	wallPanelMatWithText->SetNormalTexture(L"Resources/Textures/Panel_Normal.png");
	wallPanelMatWithText->SetLightDirection(lightDir);
	gameContext.pMaterialManager->AddMaterial(wallPanelMatWithText, 33);

	auto brownRock = new ModelMaterial();
	brownRock->SetDiffuseTexture(L"Resources/Textures/RockBrown.jpg");
	brownRock->SetNormalTexture(L"Resources/Textures/FloorPanel_Normal.png");
	brownRock->SetLightDirection(lightDir);
	gameContext.pMaterialManager->AddMaterial(brownRock, 34);

	auto greyRock = new ModelMaterial();
	greyRock->SetDiffuseTexture(L"Resources/Textures/RockGrey.jpg");
	greyRock->SetNormalTexture(L"Resources/Textures/FloorPanel_Normal.png");
	greyRock->SetLightDirection(lightDir);
	gameContext.pMaterialManager->AddMaterial(greyRock, 35);

	auto colorMaterialBlack = new ColorMaterial();
	colorMaterialBlack->SetColor(XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	gameContext.pMaterialManager->AddMaterial(colorMaterialBlack, 21);
}
