#pragma once
#include "Helpers\Singleton.h"

class TextureLoader: public Singleton<TextureLoader>
{
public:
	TextureLoader(void);
	~TextureLoader(void);

	void Initialize(const GameContext& gameContext);

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	TextureLoader(const TextureLoader &obj) = delete;
	TextureLoader& operator=(const TextureLoader& obj) = delete;

};

