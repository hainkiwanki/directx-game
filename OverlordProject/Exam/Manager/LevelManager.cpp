//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "LevelManager.h"
#include "Scenegraph\GameScene.h"
#include "..\Overlordproject\Exam\Prefabs\CustomPrefabs.h"
#include "Prefabs\SpaceBoxPrefab.h"
#include "Components\Components.h"
#include "Physx\PhysxManager.h"
#include "BulletManager.h"
#include "Scenegraph\SceneManager.h"
#include "InterfaceManager.h"

LevelManager::LevelManager(void){}

LevelManager::~LevelManager(void){}

void LevelManager::CreateLevel(GameScene * scene)
{
	//SKYBOX
	auto skybox = new SpaceBoxPrefab();
	scene->AddChild(skybox);

	//FLOOT PANEL
	auto panel = new FloorPanel();
	scene->AddChild(panel);

	float leftOffset = -55.0f;
	float rightOffset = 55.0f;

	//WALLS ON LEFT
	auto wall = new WallPanel(true, false, XMFLOAT3(leftOffset, 0.0f, 10.0f));
	scene->AddChild(wall);
	wall = new WallPanel(true, false, XMFLOAT3(leftOffset, 0.0f, -10.0f));
	scene->AddChild(wall);
	wall = new WallPanel(false, false, XMFLOAT3(leftOffset, 0.0f, 30.0f));
	scene->AddChild(wall);	
	wall = new WallPanel(false, false, XMFLOAT3(leftOffset, 0.0f, 50.0f));
	scene->AddChild(wall);
	wall = new WallPanel(false, false, XMFLOAT3(leftOffset, 0.0f, 70.0f));
	scene->AddChild(wall);
	wall = new WallPanel(true, false, XMFLOAT3(leftOffset, 0.0f, 90.0f));
	scene->AddChild(wall);
	wall = new WallPanel(false, false, XMFLOAT3(leftOffset, 0.0f, 110.0f));
	scene->AddChild(wall);
	wall = new WallPanel(false, false, XMFLOAT3(leftOffset, 0.0f, 130.0f));
	scene->AddChild(wall);

	//WALLS ON RIGHT
	wall = new WallPanel(false, true, XMFLOAT3(rightOffset, 0.0f, 10.0f));
	scene->AddChild(wall);
	wall = new WallPanel(false, true, XMFLOAT3(rightOffset, 0.0f, -10.0f));
	scene->AddChild(wall);
	wall = new WallPanel(false, true, XMFLOAT3(rightOffset, 0.0f, 30.0f));
	scene->AddChild(wall);
	wall = new WallPanel(false, true, XMFLOAT3(rightOffset, 0.0f, 50.0f));
	scene->AddChild(wall);
	wall = new WallPanel(true, true, XMFLOAT3(rightOffset, 0.0f, 70.0f));
	scene->AddChild(wall);
	wall = new WallPanel(false, true, XMFLOAT3(rightOffset, 0.0f, 90.0f));
	scene->AddChild(wall);
	wall = new WallPanel(false, true, XMFLOAT3(rightOffset, 0.0f, 110.0f));
	scene->AddChild(wall);
	wall = new WallPanel(false, true, XMFLOAT3(rightOffset, 0.0f, 130.0f));
	scene->AddChild(wall);
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto normalMat = physX->createMaterial(1.0f, 1.0f, 0.0f);

	//BACK WALL
	auto pBackWall = new GameObject();
	auto pRigidBody = new RigidBodyComponent();
	pRigidBody->SetKinematic(true);
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group1);
	pBackWall->AddComponent(pRigidBody);

	std::shared_ptr<PxGeometry> boxGeom(new PxBoxGeometry(70.0f, 10.0f, 2.0f));
	auto pCollider = new ColliderComponent(boxGeom, *normalMat, PxTransform(PxIdentity));
	pBackWall->AddComponent(pCollider);

	pBackWall->GetTransform()->Translate(0.0f, 10.0f, 130.0f);
	scene->AddChild(pBackWall);

	//FRONT WALL
	auto pFrontWall = new GameObject();
	pRigidBody = new RigidBodyComponent();
	pRigidBody->SetKinematic(true);
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group1);
	pFrontWall->AddComponent(pRigidBody);

	auto pCollider2 = new ColliderComponent(boxGeom, *normalMat, PxTransform(PxIdentity));
	pFrontWall->AddComponent(pCollider2);

	pFrontWall->GetTransform()->Translate(0.0f, 10.0f, -30.0f);
	scene->AddChild(pFrontWall);
}

void LevelManager::CreateEnemies(GameScene * scene)
{
	size_t row = 5;
	size_t column = 5;
	float xPos = -30.0f;
	float zPos = 70.0f;
	float offset = 15.0f;
	int random = 0;

	for (size_t i = 0; i < row; i++)
	{
		for (size_t j = 0; j < column; j++)
		{
			random = rand() % 100;
			EnemyPrefab* pEnemy = nullptr;
			if (random < 70)
				pEnemy = new ZombieGirl();
			else if (random < 90)
				pEnemy = new ZombieClown();
			else
				pEnemy = new ZombiePrison();
			pEnemy->GetTransform()->Translate(xPos, 7.0f, zPos);
			pEnemy->SetTag(L"enemy");
			scene->AddChild(pEnemy);
			m_pEnemies.push_back(pEnemy);
			xPos += offset;
		}
		xPos = -30.0f;
		zPos += offset;
	}
}

void LevelManager::Update()
{
	auto uiManager = InterfaceManager::GetInstance();
	if (m_pEnemies.size() > 0)
	{
		for (EnemyPrefab* enemy : m_pEnemies)
		{
			if (enemy->IsDead())
			{
				uiManager->AddScore(enemy->GetScore());
				enemy->GetScene()->RemoveChild(enemy);
				auto it = std::find(m_pEnemies.begin(), m_pEnemies.end(), enemy);
				m_pEnemies.erase(it);
			}
		}
	}
}

int LevelManager::EnemiesLeft()
{
	return (int)m_pEnemies.size();
}

void LevelManager::Reset(GameScene* scene)
{
	DeleteEnemies();
	CreateEnemies(scene);
}

void LevelManager::DeleteEnemies()
{
	for (EnemyPrefab* enemy : m_pEnemies)
	{
		enemy->GetScene()->RemoveChild(enemy);
	}
	m_pEnemies.clear();
}
