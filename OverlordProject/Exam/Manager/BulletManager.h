#pragma once
#include "Helpers\Singleton.h"

class BulletPrefab;
class EnemyBullet;
class GameScene;

class BulletManager: public Singleton<BulletManager>
{
public:
	BulletManager(void);
	~BulletManager(void);

	void SpawnBullet(const XMFLOAT3& pos, GameObject* source, GameScene* scene);
	void SpawnEnemyBullet(const XMFLOAT3& pos, GameObject* source, GameScene* scene);
	void Update();
	void Reset();

private:
	std::vector<BulletPrefab*> m_pBullets;
	std::vector<EnemyBullet*> m_pEnemyBullets;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	BulletManager(const BulletManager &obj) = delete;
	BulletManager& operator=(const BulletManager& obj) = delete;

};

