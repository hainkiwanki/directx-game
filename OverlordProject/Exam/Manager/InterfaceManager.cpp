//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "InterfaceManager.h"
#include "Scenegraph\GameScene.h"
#include "..\Overlordproject\Exam\Prefabs\CharacterPrefab.h"
#include "Graphics\TextRenderer.h"
#include "Graphics\SpriteFont.h"
#include "Content\ContentManager.h"
#include "Components\Components.h"
#include "Scenegraph\SceneManager.h"

InterfaceManager::InterfaceManager(void){}

InterfaceManager::~InterfaceManager(void){}

void InterfaceManager::Update()
{
	int health = m_pPlayerRef->GetHealth();
	wstring temp = L"";
	for (int i = 0; i < health; i++)
	{
		temp += L"|";
	}
	m_pLives = temp;
	m_pScore = to_wstring(m_Score);
	TextRenderer::GetInstance()->DrawText(m_pFont, m_pLives, XMFLOAT2{ 600.0f, 680.0f }, XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	TextRenderer::GetInstance()->DrawText(m_pFont, m_pScore, XMFLOAT2{ 10.0f, 29.0f }, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
}

void InterfaceManager::Initialize(CharacterPrefab * player, GameScene* scene)
{
	m_pPlayerRef = player;

	auto pObj = new GameObject();
	auto pScoreImg = new SpriteComponent(L"Resources/Textures/ScoreUI.png", XMFLOAT2(0.0f, 0.0f), XMFLOAT4(1, 1, 1, 1.5f));
	pObj->AddComponent(pScoreImg);
	pObj->GetTransform()->Translate(-5.0f, -5.0f, 0.95f);
	pObj->GetTransform()->Scale(0.3f, 0.3f, 0.3f);
	scene->AddChild(pObj);

	pObj = new GameObject();
	auto pHealthImg = new SpriteComponent(L"Resources/Textures/HealthUI.png", XMFLOAT2(0.0f, 0.0f), XMFLOAT4(1, 1, 1, 1.5f));
	pObj->AddComponent(pHealthImg);
	pObj->GetTransform()->Translate( 546.0f, 620.0f, 0.95f);
	pObj->GetTransform()->Scale(0.3f, 0.3f, 0.3f);
	scene->AddChild(pObj);

	m_pFont = ContentManager::Load<SpriteFont>(L"Resources/SpriteFonts/Consolas_32.fnt");
}

void InterfaceManager::AddScore(int score)
{
	m_Score += score;
}
