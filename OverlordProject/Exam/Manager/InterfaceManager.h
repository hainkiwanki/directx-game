#pragma once
#include "Helpers\Singleton.h"

class GameScene;
class CharacterPrefab;
class SpriteFont;

class InterfaceManager: public Singleton<InterfaceManager>
{
public:
	InterfaceManager(void);
	~InterfaceManager(void);

	void Update();
	void Initialize(CharacterPrefab* player, GameScene* scene);
	void AddScore(int score);
	void Reset() { m_Score = 0; }

private:
	CharacterPrefab* m_pPlayerRef;

	SpriteFont* m_pFont;
	int m_Score = 0;
	wstring m_pScore;
	wstring m_pLives;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	InterfaceManager(const InterfaceManager &obj) = delete;
	InterfaceManager& operator=(const InterfaceManager& obj) = delete;

};

