//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "BulletManager.h"
#include "Scenegraph\GameScene.h"
#include "..\Overlordproject\Exam\Prefabs\BulletPrefab.h"
#include "..\Overlordproject\Exam\Prefabs\EnemyBullet.h"
#include "Base\SoundManager.h"

BulletManager::BulletManager(void){}

BulletManager::~BulletManager(void){}

void BulletManager::SpawnBullet(const XMFLOAT3 & pos, GameObject * source, GameScene* scene)
{
	BulletPrefab* bullet = new BulletPrefab(pos, source);
	m_pBullets.push_back(bullet);
	scene->AddChild(bullet);
}

void BulletManager::SpawnEnemyBullet(const XMFLOAT3 & pos, GameObject * source, GameScene * scene)
{
	EnemyBullet* bullet = new EnemyBullet(pos, source);
	m_pEnemyBullets.push_back(bullet);
	scene->AddChild(bullet);
}

void BulletManager::Update()
{
	if (m_pBullets.size() > 0)
	{
		for (BulletPrefab* bullet : m_pBullets)
		{
			if (bullet->ToBeDeleted())
			{
				bullet->GetScene()->RemoveChild(bullet);
				auto it = std::find(m_pBullets.begin(), m_pBullets.end(), bullet);
				m_pBullets.erase(it);
			}
		}
	}
	if (m_pEnemyBullets.size() > 0)
	{
		for (EnemyBullet* bullet : m_pEnemyBullets)
		{
			if (bullet->ToBeDeleted())
			{
				bullet->GetScene()->RemoveChild(bullet);
				auto it = std::find(m_pEnemyBullets.begin(), m_pEnemyBullets.end(), bullet);
				m_pEnemyBullets.erase(it);
			}
		}
	}
}

void BulletManager::Reset()
{
	for (EnemyBullet* bullet : m_pEnemyBullets)
	{
		bullet->GetScene()->RemoveChild(bullet);
	}
	m_pEnemyBullets.clear();
	for (BulletPrefab* bullet : m_pBullets)
	{
		bullet->GetScene()->RemoveChild(bullet);
	}
	m_pBullets.clear();
}
