#pragma once
#include "Helpers\Singleton.h"

class GameScene;
class EnemyPrefab;

class LevelManager: public Singleton<LevelManager>
{
public:
	LevelManager(void);
	~LevelManager(void);

	void CreateLevel(GameScene* scene);
	void CreateEnemies(GameScene* scene);

	void Update();
	int EnemiesLeft();
	void Reset(GameScene* scene);

private:
	std::vector<EnemyPrefab*> m_pEnemies;
	void DeleteEnemies();
private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	LevelManager(const LevelManager &obj) = delete;
	LevelManager& operator=(const LevelManager& obj) = delete;

};

