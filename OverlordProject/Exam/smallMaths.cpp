#include "smallMaths.h"

float smoll::Lerp(float beginPos, float endPos, float time)
{
	return ((1 - time) * beginPos) + (time * endPos);
}
