//=============================================================================
//// Shader uses position and texture
//=============================================================================
SamplerState samPoint
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Mirror;
    AddressV = Mirror;
};

Texture2D gTexture;

float gContrast
<
	string UIName = "Contrast";
	string UIWidget = "slider";
	float UIMin = 0;
	float UIMax = 10;
	float UIStep = 0.01;
>  = 0.0f;

bool gUse4Bit = false;
bool gInverse = false;

/// Create Depth Stencil State (ENABLE DEPTH WRITING)
DepthStencilState Depth
{
	DepthEnable = true;
	DepthWriteMask = ALL;
};
/// Create Rasterizer State (Backface culling)
RasterizerState BackCulling
{
	CullMode = Back;
};


//IN/OUT STRUCTS
//--------------
struct VS_INPUT
{
    float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;

};

struct PS_INPUT
{
    float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD1;
};

float Set4BitColor(float color)
{
	float value = color;
	float lowerBorder = 0.0f;
	float upperBorder = 85.0f;
	for(int i = 0; i < 4; i++)
	{
		if(lowerBorder <= value && value <= upperBorder)
		{
			if((value - lowerBorder) <= (upperBorder - value))
			{
				value = lowerBorder;
			}
			else
			{
				value = upperBorder;
			}
			break;
		}
		lowerBorder += upperBorder;
		upperBorder += 85;
	}

	return value;
}

float3 ConvertTo4BitColor(float3 color)
{
	float r = Set4BitColor(color.r * 255) / 255;
	float g = Set4BitColor(color.g * 255) / 255;
	float b = Set4BitColor(color.b * 255) / 255;

	return float3(r, g, b);
}

//VERTEX SHADER
//-------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;
	// Set the Position
    output.Position = float4(input.Position, 1);
	// Set the TexCoord
	output.TexCoord = input.TexCoord;

	return output;
}

//PIXEL SHADER
//------------
float4 PS(PS_INPUT input): SV_Target
{
    float4 rgb = gTexture.Sample(samPoint, input.TexCoord).rgba;

    float3 result = ((rgb.rgb - 0.5f) * gContrast) + 0.5;

	float3 newresult = result;
	if(gUse4Bit)
		newresult = ConvertTo4BitColor(result);

	if(gInverse)
		newresult = 1 - newresult;

    return float4(newresult, 1);
}

//TECHNIQUE
//---------
technique11 Grayscale
{
    pass P0
    {
        SetRasterizerState(BackCulling);
		SetDepthStencilState(Depth, 0);
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}

