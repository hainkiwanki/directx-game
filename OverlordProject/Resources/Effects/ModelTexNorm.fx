//DX10 - FLAT SHADER
//Digital Arts & Entertainment
//FirstName: Kevin
//LastName: Radino
//Class: 2DAE01

//GLOBAL VARIABLES
//****************
float4x4 gMatrixWorldViewProj : WORLDVIEWPROJECTION;
float4x4 gMatrixWorld : WORLD;
float4x4 gViewInv: VIEWINVERSE;

Texture2D gTextureDiffuse<
	string UIName = "Diffuse Texture";
	string ResourceName = "CobbleStone_DiffuseMap.dds";
	string ResourceType = "2D";
>;

Texture2D gNormalMapTexture<
	string UIName = "Normal Map";
	string ResourceName = "CobbleStone_NormalMap.dds";
	string ResourceType = "2D";
>;

float gNormalIntensity<
	string UIName = "Normal Intensity";
	string UIWidget = "Slider";
	float UIMin = 0.0f;
	float UIMax = 1.0f;
	float UIStep = 0.01f;
> = 1.0f;

float3 gLightDirection : DIRECTION<
	string UIName = "Light Direction";
	string Object = "TargetLight";
> = float3(-0.57f, -0.57f, -0.57f);

int gShininess <
	string UIName = "Shininess";
	string UIWidget = "Slider";
	float UIMin = 0.f;
	float UIMax = 100.f;
	float UIStep = 1.f;
> = 15.f;

float4 gAmbientColor <
	string UIName = "Ambient Color";
	string UIWidget = "Color";
> = float4(0.5f,0.5f,0.5f,1.0f);

float gAmbientIntensity<
	string UIName = "Ambient Intensity";
	string UIWidget = "Slider";
	float UIMin = 0.0f;
	float UIMax = 1.0f;
	float UIStep = 0.01f;
> = 1.0f;

float4 gSpecularColor<
	string UIName = "Specular Color";
	string UIWidget = "Color";
> = float4(1.0f, 1.0f, 1.0f, 1.0f);

float gSpecularIntensity <
	string UIName = "Specular Intensity";
	string UIWidget = "Slider";
	float UIMin = 0.0f;
	float UIMax = 1.0f;
	float UIStep = 0.1f;
> = 1.0f;

float4 gColorDiffuse : COLOR<
	string UIName = "Diffuse Color";
	string UIWidget = "Color";
> = float4(1.0, 1.0, 1.0, 1.0);

float gDiffuseIntensity <
	string UIName = "Diffuse Intensity";
	string UIWidget = "Slider";
	float UIMin = 0.0f;
	float UIMax = 1.0f;
	float UIStep = 0.1f;
> = 0.5f;

float gFlipYNormal <
	string UIName = "Flip Y Normal";
	string UIWidget = "Slider";
	float UIMin = -1.0f;
	float UIMax = 1.0f;
	float UIStep = 1.0f;
> = 1.f;

float gFlipXNormal <
	string UIName = "Flip X Normal";
	string UIWidget = "Slider";
	float UIMin = -1.0f;
	float UIMax = 1.0f;
	float UIStep = 1.0f;
> = 1.f;
//STATES
//******
RasterizerState gRS_NoCulling { CullMode = NONE; };
SamplerState gDiffuseSamplerState
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};
SamplerState gNormalMapState
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

//IN/Out Stucts
struct VS_INPUT
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD0;
	float3 Tangent : TANGENT;
};

struct VS_OUTPUT
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD0;
	float3 Tangent : TANGENT;
	float3 WorldPosition : POSITION;
};

//MAIN VERTEX SHADER
//******************
VS_OUTPUT MainVS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Position = mul(float4(input.Position.xyz, 1.0), gMatrixWorldViewProj);
	output.Normal = mul(input.Normal, (float3x3)gMatrixWorld);
	output.TexCoord = input.TexCoord;
	output.Tangent = mul(normalize(input.Tangent), (float3x3)gMatrixWorld);
	output.WorldPosition = mul(float4(input.Position, 1), gMatrixWorld).xyz;
	return output;
}


//MAIN PIXEL SHADER
//*****************
float4 MainPS(VS_OUTPUT input) : SV_TARGET 
{
	//DIFFUSE
	float3 normal = normalize(input.Normal);
	float3 tangent = normalize(input.Tangent);
	float3 binormal = normalize(cross(tangent, normal));

	float3x3 localAxis = float3x3(tangent, binormal, normal);

	float3 sampledNormal = gNormalMapTexture.Sample(gNormalMapState, input.TexCoord).rgb*2.0f-1.0f;
	sampledNormal.xy *= gNormalIntensity;
	sampledNormal.y *= gFlipYNormal;
	sampledNormal.x *= gFlipXNormal;

	float3 newNormal = mul(sampledNormal, localAxis);

	float diffuseLightValue = max(dot(newNormal, -gLightDirection), 0);
	float3 diffuse = gTextureDiffuse.Sample(gDiffuseSamplerState, input.TexCoord) * diffuseLightValue;

	float4 diffuseCol = float4(diffuse, 1.f) * gColorDiffuse;
	diffuseCol *= gDiffuseIntensity;

	//SPECULAR
	float3 viewDir = normalize(input.WorldPosition - gViewInv[3].xyz);
	float3 reflected = reflect(normalize(gLightDirection), newNormal);
	float specStr = pow(max(dot(-viewDir, reflected), 0), gShininess);
	float3 specCol = gSpecularColor.rgb * specStr * gSpecularIntensity;

	//AMBIENT
	float4 albedo = gTextureDiffuse.Sample( gDiffuseSamplerState, input.TexCoord );
   	diffuse = diffuseLightValue * albedo.rgb * diffuse;

   	float3 ambient = gAmbientColor.rgb * albedo * gAmbientIntensity;

	return float4(diffuseCol + specCol + ambient, 1.0f);
}


//TECHNIQUES
//**********
technique10 DefaultTechnique {
	pass p0 {
		SetRasterizerState(gRS_NoCulling);	
		SetVertexShader(CompileShader(vs_4_0, MainVS()));
		SetPixelShader(CompileShader(ps_4_0, MainPS()));
	}
}