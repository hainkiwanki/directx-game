//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "MainGame.h"
#include "Base\GeneralStructs.h"
#include "Scenegraph\SceneManager.h"
#include "Physx\PhysxProxy.h"
#include "Diagnostics\DebugRenderer.h"
#include "Scenegraph\GameScene.h"
#include "Exam\Manager\TextureLoader.h"

#define EXAM


#ifdef EXAM
#include "Exam\Scenes\MenuScene.h"
#include "Exam\Scenes\SpaceScene.h"
#endif

MainGame::MainGame(void)
{
}


MainGame::~MainGame(void)
{
}

//Game is preparing
void MainGame::OnGamePreparing(GameSettings& gameSettings)
{
	UNREFERENCED_PARAMETER(gameSettings);
	srand((unsigned int)time(NULL));
}

void MainGame::Initialize()
{
#ifdef EXAM
	DebugRenderer::ToggleDebugRenderer();
	SceneManager::GetInstance()->AddGameScene(new MenuScene());
	SceneManager::GetInstance()->AddGameScene(new SpaceScene());
#endif
}

LRESULT MainGame::WindowProcedureHook(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(hWnd);
	UNREFERENCED_PARAMETER(message);
	UNREFERENCED_PARAMETER(wParam);
	UNREFERENCED_PARAMETER(lParam);
	return -1;
}
