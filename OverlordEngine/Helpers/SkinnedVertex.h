#pragma once
#include "stdafx.h"

struct SkinnedVertex
{
	SkinnedVertex(XMFLOAT3 pos, XMFLOAT3 normal, XMFLOAT4 col, float blendWeight = 1.f, float blendWeight2 = 0.f) :
		TransformedVertex(pos, normal, col),
		OriginalVertex(pos, normal, col),
		BlendWeight(blendWeight),
		BlendWeight2(blendWeight2)
	{}

	float BlendWeight;
	float BlendWeight2;
	VertexPosNormCol TransformedVertex;
	VertexPosNormCol OriginalVertex;
};