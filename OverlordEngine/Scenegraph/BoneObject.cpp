#include "stdafx.h"
#include "BoneObject.h"
#include "..\Components\ModelComponent.h"
#include "..\Components\TransformComponent.h"

BoneObject::BoneObject(int boneId, int matId, float length) :
	m_BondId(boneId), m_MaterialId(matId), m_Length(length)
{
}

BoneObject::~BoneObject()
{
}

void BoneObject::AddBone(BoneObject * pBone)
{
	XMFLOAT3 translate{ 0.0f, 0.0f, -m_Length };
	pBone->GetTransform()->Translate(XMLoadFloat3(&translate));
	AddChild(pBone);
}

XMFLOAT4X4 BoneObject::GetBindPose() const
{
	return m_BindPose;
}

void BoneObject::CalculateBindPose()
{
	auto world = XMLoadFloat4x4(&GetTransform()->GetWorld());
	auto determinant = XMMatrixDeterminant(world);
	auto inverse = XMMatrixInverse(&determinant, world);
	XMStoreFloat4x4(&m_BindPose, inverse);

	for (auto child : GetChildren<BoneObject>())
	{
		child->CalculateBindPose();
	}
}

void BoneObject::Initialize(const GameContext & gameContext)
{
	auto model = new ModelComponent(L"Resources/Meshes/Bone.ovm");
	model->SetMaterial(m_MaterialId);

	auto bone = new GameObject();
	bone->AddComponent(model);
	bone->GetTransform()->Scale(XMFLOAT3(m_Length, m_Length, m_Length));
	AddChild(bone);
}
