#pragma once
#include "..\Scenegraph\GameObject.h"



class BoneObject : public GameObject
{
public:
	BoneObject(int boneId, int matId, float length = 5.f);
	~BoneObject();

	void AddBone(BoneObject* pBone);
	XMFLOAT4X4 GetBindPose() const;
	void CalculateBindPose();

protected:
	virtual void Initialize(const GameContext& gameContext);

private:
	float m_Length;
	int m_BondId;
	int m_MaterialId;
	XMFLOAT4X4 m_BindPose;

private:
	BoneObject(const BoneObject& other);
	BoneObject& operator=(const BoneObject& other);
};
