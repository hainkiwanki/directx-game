#include "stdafx.h"
#include "SpriteFontLoader.h"
#include "../Helpers/BinaryReader.h"
#include "../Content/ContentManager.h"
#include "../Graphics/TextRenderer.h"
#include "../Graphics/TextureData.h"

SpriteFontLoader::SpriteFontLoader()
{

}


SpriteFontLoader::~SpriteFontLoader()
{
}

#pragma warning( disable : 4702 )
SpriteFont* SpriteFontLoader::LoadContent(const wstring& assetFile)
{
	auto pBinReader = new BinaryReader(); //Prevent memory leaks
	pBinReader->Open(assetFile);

	if (!pBinReader->Exists())
	{
		delete pBinReader;
		Logger::LogFormat(LogLevel::Error, L"SpriteFontLoader::LoadContent > Failed to read the assetFile!\nPath: \'%s\'", assetFile.c_str());

		return nullptr;
	}

	//See BMFont Documentation for Binary Layout

	//Parse the Identification bytes (B,M,F)
	//If Identification bytes doesn't match B|M|F,
	//Log Error (SpriteFontLoader::LoadContent > Not a valid .fnt font) &
	//return nullpt
	{
		char c = pBinReader->Read<char>();
		bool inCorrect{ false };
		if (c != 'B') inCorrect = true;
		c = pBinReader->Read<char>();
		if (c != 'M') inCorrect = true;
		c = pBinReader->Read<char>();
		if (c != 'F') inCorrect = true;
		if (inCorrect)
		{
			Logger::LogFormat(LogLevel::Error, L"SpriteFontLoader::LoadContent > Not a valid .fnt font!\n");
			return nullptr;
		}
	}
	//Parse the version (version 3 required)
	//If version is < 3,
	//Log Error (SpriteFontLoader::LoadContent > Only .fnt version 3 is supported)
	//return nullptr
	{
		char c = pBinReader->Read<char>();
		if (c != 3)
		{
			Logger::LogFormat(LogLevel::Error, L"SpriteFontLoader::LoadContent > Only .fnt version 3 is supported.\n");
			return nullptr;
		}
	}
	//Valid .fnt file
	auto pSpriteFont = new SpriteFont();
	//SpriteFontLoader is a friend class of SpriteFont
	//That means you have access to its privates (pSpriteFont->m_FontName = ... is valid)

	//**********
	// BLOCK 0 *
	//**********
	{
		//Retrieve the blockId and blockSize
		char blockId = pBinReader->Read<char>();
		int blockSize = pBinReader->Read<int>();
		UNREFERENCED_PARAMETER(blockId);
		UNREFERENCED_PARAMETER(blockSize);
		//Retrieve the FontSize (will be -25, BMF bug) [SpriteFont::m_FontSize]
		pSpriteFont->m_FontSize = pBinReader->Read<short>();
		//Move the binreader to the start of the FontName [BinaryReader::MoveBufferPosition(...) or you can set its position using BinaryReader::SetBufferPosition(...))
		pBinReader->MoveBufferPosition(12);
		//Retrieve the FontName [SpriteFont::m_FontName]
		pSpriteFont->m_FontName = pBinReader->ReadNullString();
	}

	//**********
	// BLOCK 1 *
	//**********
	{
		//Retrieve the blockId and blockSize
		char blockId = pBinReader->Read<char>();
		int blockSize = pBinReader->Read<int>();
		UNREFERENCED_PARAMETER(blockId);
		UNREFERENCED_PARAMETER(blockSize);
		//Retrieve Texture Width & Height [SpriteFont::m_TextureWidth/m_TextureHeight]
		pBinReader->MoveBufferPosition(4);
		pSpriteFont->m_TextureWidth = pBinReader->Read<short>();
		pSpriteFont->m_TextureHeight = pBinReader->Read<short>();
		//Retrieve PageCount
		int pageCount = pBinReader->Read<short>();
		//> if pagecount > 1
		if (pageCount > 1)
		{
			//> Log Error (SpriteFontLoader::LoadContent > SpriteFont (.fnt): Only one texture per font allowed)
			Logger::LogFormat(LogLevel::Error, L"SpriteFontLoader::LoadContent > SpriteFont (.fnt): Only one texture per font allowed.\n");
		}
		//Advance to Block2 (Move Reader)
		pBinReader->MoveBufferPosition(5);
	}

	//**********
	// BLOCK 2 *
	//**********
	{
		//Retrieve the blockId and blockSize
		char blockId = pBinReader->Read<char>();
		int blockSize = pBinReader->Read<int>();
		UNREFERENCED_PARAMETER(blockId);
		UNREFERENCED_PARAMETER(blockSize);
		//Retrieve the PageName (store Local)
		wstring pageName = pBinReader->ReadNullString();
		//	> If PageName is empty
		if (pageName.empty())
		{
			//	> Log Error (SpriteFontLoader::LoadContent > SpriteFont (.fnt): Invalid Font Sprite [Empty])
			Logger::LogFormat(LogLevel::Error, L"SpriteFontLoader::LoadContent > SpriteFont (.fnt): Invalid Font Sprite [Empty]\n ");
		}
		//>Retrieve texture filepath from the assetFile path
		//> (ex. c:/Example/somefont.fnt => c:/Example/) [Have a look at: wstring::rfind()]
		auto found = assetFile.rfind('/');
		wstring path{};
		if (found != wstring::npos)
		{
			path = assetFile.substr(0, found + 1);
		}
		//>Use path and PageName to load the texture using the ContentManager [SpriteFont::m_pTexture]
		//> (ex. c:/Example/ + 'PageName' => c:/Example/somefont_0.png)
		wstring realPath{ path + pageName };
		pSpriteFont->m_pTexture = ContentManager::Load<TextureData>(realPath);
	}

	//**********
	// BLOCK 3 *
	//**********
	{
		//Retrieve the blockId and blockSize
		char blockId = pBinReader->Read<char>();
		int blockSize = pBinReader->Read<int>();
		UNREFERENCED_PARAMETER(blockId);
		//Retrieve Character Count (see documentation)
		auto numChar = blockSize / 20;
		//Retrieve Every Character, For every Character:
		for (int i = 0; i < numChar; i++)
		{
			//> Retrieve CharacterId (store Local) and cast to a 'wchar_t'
			wchar_t characterId = (wchar_t)pBinReader->Read<int>();
			//> Check if CharacterId is valid (SpriteFont::IsCharValid), Log Warning and advance to next character if not valid
			if (!pSpriteFont->IsCharValid(characterId))
			{
				Logger::LogFormat(LogLevel::Warning, L" Warning and advance to next character if not valid\n ");
			}
			else
			{
				//> Retrieve the corresponding FontMetric (SpriteFont::GetMetric) [REFERENCE!!!]
				auto metric = &pSpriteFont->GetMetric(characterId);
				//> Set IsValid to true [FontMetric::IsValid]
				metric->IsValid = true;
				//> Set Character (CharacterId) [FontMetric::Character]
				metric->Character = characterId;
				//> Retrieve Xposition (store Local)
				float xPos = pBinReader->Read<short>();
				//> Retrieve Yposition (store Local)
				float yPos = pBinReader->Read<short>();
				//> Retrieve & Set Width [FontMetric::Width]
				metric->Width = pBinReader->Read<short>();
				//> Retrieve & Set Height [FontMetric::Height]
				metric->Height = pBinReader->Read<short>();
				//> Retrieve & Set OffsetX [FontMetric::OffsetX]
				metric->OffsetX = pBinReader->Read<short>();
				//> Retrieve & Set OffsetY [FontMetric::OffsetY]
				metric->OffsetY = pBinReader->Read<short>();
				//> Retrieve & Set AdvanceX [FontMetric::AdvanceX]
				metric->AdvanceX = pBinReader->Read<short>();
				//> Retrieve & Set Page [FontMetric::Page]
				metric->Page = pBinReader->Read<char>();
				//> Retrieve Channel (BITFIELD!!!) 
				//	> See documentation for BitField meaning [FontMetrix::Channel]
				int tmp = (int)pBinReader->Read<char>();
				switch (tmp)
				{
				case 1:
					//blue
					metric->Channel = 2;
					break;
				case 2:
					//green
					metric->Channel = 1;
					break;
				case 4:
					//red
					metric->Channel = 0;
					break;
				case 8:
					metric->Channel = 3;
					break;
				}
				//> Calculate Texture Coordinates using Xposition, Yposition, TextureWidth & TextureHeight [FontMetric::TexCoord]
				auto textureSize = pSpriteFont->m_pTexture->GetDimension();
				metric->TexCoord = XMFLOAT2{ xPos / textureSize.x, yPos / textureSize.y };
			}
		}
	}

	delete pBinReader;
	return pSpriteFont;

#pragma warning(default:4702)  
}

void SpriteFontLoader::Destroy(SpriteFont* objToDestroy)
{
	SafeDelete(objToDestroy);
}