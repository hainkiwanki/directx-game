#include "stdafx.h"
#include "SkyBoxMaterial.h"
#include "../Content/ContentManager.h"
#include "TextureData.h"

ID3DX11EffectShaderResourceVariable* SkyBoxMaterial::m_pCubeMapSRV = nullptr;

SkyBoxMaterial::SkyBoxMaterial() :
	Material(L"Resources/Effects/SkyBox.fx"),
	m_pCubeMap(nullptr)
{
}

SkyBoxMaterial::~SkyBoxMaterial()
{
}

void SkyBoxMaterial::SetDiffuseTexture(const wstring & assetFile)
{
	m_pCubeMap = ContentManager::Load<TextureData>(assetFile);
}


void SkyBoxMaterial::LoadEffectVariables()
{
	m_pCubeMapSRV = m_pEffect->GetVariableByName("m_CubeMap")->AsShaderResource();
}

void SkyBoxMaterial::UpdateEffectVariables(const GameContext & gameContext, ModelComponent * pModelComponent)
{
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pModelComponent);
	
	m_pCubeMapSRV->SetResource(m_pCubeMap->GetShaderResourceView());
}
