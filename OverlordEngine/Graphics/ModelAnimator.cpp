//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "ModelAnimator.h"
#include "../Diagnostics/Logger.h"


ModelAnimator::ModelAnimator(MeshFilter* pMeshFilter) :
	m_pMeshFilter(pMeshFilter),
	m_Transforms(vector<XMFLOAT4X4>()),
	m_IsPlaying(false),
	m_Reversed(false),
	m_ClipSet(false),
	m_TickCount(0),
	m_AnimationSpeed(1.0f)
{
	SetAnimation(0);
}


ModelAnimator::~ModelAnimator()
{
}

void ModelAnimator::SetAnimation(UINT clipNumber)
{
	//Set m_ClipSet to false
	m_ClipSet = false;
	m_Done = false;
	//Check if clipNumber is smaller than the actual m_AnimationClips vector size
	//If not,
	//	Call Reset
	//	Log a warning with an appropriate message
	//	return
	//else
	//	Retrieve the AnimationClip from the m_AnimationClips vector based on the given clipNumber
	//	Call SetAnimation(AnimationClip clip)
	if (clipNumber >= m_pMeshFilter->m_AnimationClips.size())
	{
		Reset();
		Logger::LogFormat(LogLevel::Warning, L"ModelAnimator::SetAnimation() > Warning. Not the right clipsize ");
		return;
	}
	else
	{
		SetAnimation(m_pMeshFilter->m_AnimationClips[clipNumber]);
	}
}

void ModelAnimator::SetAnimation(wstring clipName)
{
	//Set m_ClipSet to false
	m_ClipSet = false;
	m_Done = false;
	//Iterate the m_AnimationClips vector and search for an AnimationClip with the given name (clipName)
	//If found,
	//	Call SetAnimation(Animation Clip) with the found clip
	//Else
	//	Call Reset
	//	Log a warning with an appropriate message
	for (size_t i = 0; i < m_pMeshFilter->m_AnimationClips.size(); i++)
	{
		if (m_pMeshFilter->m_AnimationClips[i].Name == clipName)
		{
			SetAnimation(m_pMeshFilter->m_AnimationClips[i]);
		}
		else
		{
			Reset();
			Logger::LogFormat(LogLevel::Warning, L"ModelAnimator::SetAnimation() > Warning. No such clip under said name.");
		}
	}
}

void ModelAnimator::SetAnimation(AnimationClip clip)
{
	//Set m_ClipSet to true
	m_ClipSet = true;
	//Set m_CurrentClip
	m_CurrentClip = clip;

	m_Done = false;

	//Call Reset(false)
	Reset(false);
}

void ModelAnimator::Reset(bool pause)
{
	//If pause is true, set m_IsPlaying to false
	if (pause)
	{
		m_IsPlaying = false;
	}
	//Set m_TickCount to zero
	m_TickCount = 0;
	//Set m_AnimationSpeed to 1.0f
	m_AnimationSpeed = 1.0f;

	//If m_ClipSet is true
	if (m_ClipSet)
	{
		//	Retrieve the BoneTransform from the first Key from the current clip (m_CurrentClip)
		auto transforms = m_CurrentClip.Keys.at(0).BoneTransforms;
		//	Refill the m_Transforms vector with the new BoneTransforms (have a look at vector::assign)
		m_Transforms.assign(transforms.begin(), transforms.end());
	}
	//Else
	else
	{
		//	Create an IdentityMatrix 
		XMFLOAT4X4 matrix;
		XMStoreFloat4x4(&matrix, XMMatrixIdentity());
		//	Refill the m_Transforms vector with this IdenityMatrix (Amount = BoneCount) (have a look at vector::assign)
		m_Transforms.assign(m_pMeshFilter->m_BoneCount, matrix);
	}
}

void ModelAnimator::Update(const GameContext& gameContext)
{
	//We only update the transforms if the animation is running and the clip is set
	if (m_IsPlaying && m_ClipSet)
	{
		//1. 
		//Calculate the passedTicks (see the lab document)
		auto passedTicks = gameContext.pGameTime->GetElapsed() * m_CurrentClip.TicksPerSecond * m_AnimationSpeed;
		//Make sure that the passedTicks stay between the m_CurrentClip.Duration bounds (fmod)
		passedTicks = fmod(passedTicks, m_CurrentClip.Duration);

		//2. 
		//IF m_Reversed is true
		if (m_Reversed)
		{
			//	Subtract passedTicks from m_TickCount
			m_TickCount -= passedTicks;
			//	If m_TickCount is smaller than zero, add m_CurrentClip.Duration to m_TickCount
			if (m_TickCount <= 0.0f)
			{
				if (m_Loop)
				{
					m_TickCount += m_CurrentClip.Duration;
				}
				m_Done = true;
			}
		}
		//ELSE
		else
		{
			//	Add passedTicks to m_TickCount
			m_TickCount += passedTicks;
			//	if m_TickCount is bigger than the clip duration, subtract the duration from m_TickCount
			if (m_TickCount > m_CurrentClip.Duration)
			{
				if (m_Loop)
				{
					m_TickCount -= m_CurrentClip.Duration;
				}
				m_Done = true;
			}
		}
		//3.
		//Find the enclosing keys
		AnimationKey keyA, keyB;
		//Iterate all the keys of the clip and find the following keys:
		//keyA > Closest Key with Tick before/smaller than m_TickCount
		//keyB > Closest Key with Tick after/bigger than m_TickCount
		auto found = find_if(m_CurrentClip.Keys.begin(), m_CurrentClip.Keys.end(), [this](const AnimationKey& k)
		{
			if (k.Tick > m_TickCount)
				return true;	
			else
				return false;
		});
		keyB = *found;
		keyA = *(found - 1);

		//4.
		//Interpolate between keys
		//Figure out the BlendFactor (See lab document)
		auto blendA = abs((keyA.Tick - m_TickCount) / (keyB.Tick - keyA.Tick));
		auto blendB = 1.0f - blendA;
		//Clear the m_Transforms vector
		m_Transforms.clear();
		//FOR every boneTransform in a key (So for every bone)
		for (int i = 0; i < keyA.BoneTransforms.size(); i++)
		{
			//	Retrieve the transform from keyA (transformA)
			auto transformA = XMLoadFloat4x4(&keyA.BoneTransforms[i]);
			// 	Retrieve the transform from keyB (transformB)
			auto transformB = XMLoadFloat4x4(&keyB.BoneTransforms[i]);
			//	Decompose both transforms	
			XMVECTOR sA, rA, tA;
			XMMatrixDecompose(&sA, &rA, &tA, transformA);
			XMVECTOR sB, rB, tB;
			XMMatrixDecompose(&sB, &rB, &tB, transformB);

			//	Lerp between all the transformations (Position, Scale, Rotation)
			XMVECTOR scale = XMVectorLerp(sA, sB, blendA);
			XMVECTOR transform = XMVectorLerp(tA, tB, blendA);
			XMVECTOR rotate = XMQuaternionSlerp(rA, rB, blendA);

			//	Compose a transformation matrix with the lerp-results
			XMFLOAT4 scalef;
			XMStoreFloat4(&scalef, scale);
			XMFLOAT4 transformf;
			XMStoreFloat4(&transformf, transform);
			XMMATRIX result =
				XMMatrixScaling(scalef.x, scalef.y, scalef.z)*
				XMMatrixRotationQuaternion(rotate)*
				XMMatrixTranslation(transformf.x, transformf.y, transformf.z);
			XMFLOAT4X4 matrix;
			XMStoreFloat4x4(&matrix, result);
			//	Add the resulting matrix to the m_Transforms vector
			m_Transforms.push_back(matrix);
		}
	}
}
