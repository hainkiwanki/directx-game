#include "stdafx.h"
#include "SkyBoxPrefab.h"
#include "..\Graphics\SkyBoxMaterial.h"

SkyBoxPrefab::SkyBoxPrefab() :
	m_ModelComponent(nullptr)
{
}


SkyBoxPrefab::~SkyBoxPrefab()
{
	
}

void SkyBoxPrefab::Initialize(const GameContext & gameContext)
{
	m_ModelComponent = new ModelComponent(L"Resources\\Meshes\\Box.ovm");
	auto cubeMapMat = new SkyBoxMaterial();
	cubeMapMat->SetDiffuseTexture(L"Resources\\Textures\\SkyBox.dds");
	gameContext.pMaterialManager->AddMaterial(cubeMapMat, 0);
	m_ModelComponent->SetMaterial(0);

	AddComponent(m_ModelComponent);

}
