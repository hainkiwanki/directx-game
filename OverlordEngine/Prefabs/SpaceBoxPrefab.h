#pragma once

#include "..\Scenegraph\GameObject.h"
#include "..\Components\ModelComponent.h"

class SpaceBoxPrefab : public GameObject
{
public:
	SpaceBoxPrefab();
	~SpaceBoxPrefab();
protected:

	virtual void Initialize(const GameContext& gameContext);

private:
	ModelComponent * m_ModelComponent;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	SpaceBoxPrefab(const SpaceBoxPrefab& yRef);
	SpaceBoxPrefab& operator=(const SpaceBoxPrefab& yRef);
};

