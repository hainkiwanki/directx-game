#pragma once

#include "..\Scenegraph\GameObject.h"
#include "..\Components\ModelComponent.h"

class SkyBoxPrefab : public GameObject
{
public:
	SkyBoxPrefab();
	~SkyBoxPrefab();
protected:

	virtual void Initialize(const GameContext& gameContext);

private:
	ModelComponent * m_ModelComponent;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	SkyBoxPrefab(const SkyBoxPrefab& yRef);
	SkyBoxPrefab& operator=(const SkyBoxPrefab& yRef);
};

