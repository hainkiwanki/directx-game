#include "stdafx.h"
#include "SpaceBoxPrefab.h"
#include "..\Graphics\SkyBoxMaterial.h"

SpaceBoxPrefab::SpaceBoxPrefab() :
	m_ModelComponent(nullptr)
{
}


SpaceBoxPrefab::~SpaceBoxPrefab()
{
	
}

void SpaceBoxPrefab::Initialize(const GameContext & gameContext)
{
	m_ModelComponent = new ModelComponent(L"Resources\\Meshes\\Box.ovm");
	auto cubeMapMat = new SkyBoxMaterial();
	cubeMapMat->SetDiffuseTexture(L"Resources\\Textures\\SpaceSkyBox.dds");
	gameContext.pMaterialManager->AddMaterial(cubeMapMat, 11);
	m_ModelComponent->SetMaterial(11);

	AddComponent(m_ModelComponent);

}
